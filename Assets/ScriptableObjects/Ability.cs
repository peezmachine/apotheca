﻿using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Ability")]
public class Ability : ScriptableObject {


    [SerializeField] string _name;
    [SerializeField] Targeter _targeter;


    public Targeter Targeter {
        get {
            return _targeter;
        }
    }

    //public Targeter Targeter;



}