﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Apotheca/Effect/Move Line Effect")]
public class MoveLineEffect : Effect {
    public override EffectType Type { get { return EffectType.MoveLine; } }



    [SerializeField] TargeterBase _origin;
    [SerializeField] TargeterBase _direction;
    [SerializeField] int _amount = 1;
    [SerializeField] bool _wrapAroundEdge = true;


    public TargeterBase Origin {
        get {
            return _origin;
        }
    }

    public TargeterBase Direction {
        get {
            return _direction;
        }
    }

    public int Amount {
        get {
            return _amount;
        }

    }

    public bool WrapAroundEdge {
        get {
            return _wrapAroundEdge;
        }
    }

}
