﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TODO -- consider moving the "source targeter" field to this class, since out effect system
// doesn't have implicit sources.
// ^^^ can't really do this! You could, but some Effects, like the MoveEffect, don't really have 
// a single source, so it's actually on a class-by-class basis

//[System.Serializable]
public abstract class Effect : ScriptableObject {
    public enum EffectType { Move, MoveLine, Rotate, Tempest }
    public abstract EffectType Type { get; }
    //[SerializeField] Targeter _sourceTargeter;

    //public Targeter SourceTargeter { get { return _sourceTargeter; } }
}

//[CreateAssetMenu(menuName = "Apotheca/Effect/Target Effect")]
//public class TargetEffect : Effect {
    

//    [SerializeField] int _minRange;
//    [SerializeField] int _maxRange; //0 for unlimited, for now. Might define a Range object if needed later
//    [SerializeField] bool _allowDiagonal; //if true, count diagonal steps as 1 range, not 2
//    [SerializeField] Targeter _nextTargeter;

//}

