﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Effect/Rotate Effect")]
public class RotateEffect : Effect {
    public override EffectType Type { get { return EffectType.Rotate; } }



    public enum DirectionType { Clockwise, CounterClockwise}

    [SerializeField] TargeterBase _source;
    [SerializeField] DirectionType _direction;

    public TargeterBase Source {
        get {
            return _source;
        }

        set {
            _source = value;
        }
    }

    public DirectionType Direction {
        get {
            return _direction;
        }

        set {
            _direction = value;
        }
    }
}
