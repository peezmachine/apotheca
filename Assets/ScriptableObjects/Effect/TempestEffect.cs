﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Effect/Tempest Effect")]
public class TempestEffect : Effect {
    public override EffectType Type { get { return EffectType.Tempest; } }




    [SerializeField] Targeter _direction;


    public Targeter Direction {
        get {
            return _direction;
        }
    }
}
