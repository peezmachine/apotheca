﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Effect/Move Effect")]
public class MoveEffect : Effect {
    public override EffectType Type { get { return EffectType.Move; } }
 
    [SerializeField] List<MoveInfo> _moves;

    public List<MoveInfo> Moves {
        get {
            return _moves;
        }
    }

    [System.Serializable]
    public struct MoveInfo {
        [SerializeField] TargeterBase _source;
        [SerializeField] TargeterBase _destination;

        public TargeterBase Source {
            get {
                return _source;
            }
        }

        public TargeterBase Destination {
            get {
                return _destination;
            }
        }
    }
}
