﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Apotheca/Apothecary Library")]
public class ApothecaryLibrary : ScriptableObject {
    [SerializeField] List<ApothecaryData> _apothecaries;

    public List<ApothecaryData> Apothecaries {
        get {
            return _apothecaries;
        }
    }
}
