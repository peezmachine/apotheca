﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Apothecary")]
public class ApothecaryData : ScriptableObject {
    [SerializeField] Ability _ability;
    [SerializeField] string _name;

    public string Name {
        get {
            return _name;
        }

        set {
            _name = value;
        }
    }

    public Ability Ability {
        get {
            return _ability;
        }
    }
}