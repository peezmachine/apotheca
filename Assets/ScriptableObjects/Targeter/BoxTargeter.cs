﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Targeter/Box Targeter")]
public class BoxTargeter : Targeter<BoxTargeter.BoxTargetInfo> {
    public override TargeterType Type { get { return TargeterType.Box; } }
    public bool AllMustBeOnBoard = true;


    public class BoxTargetInfo {
        public Vector2Int Center { get; private set;}
        public ICollection<Vector2Int> Targets { get; private set; } //all the affected locations


        public BoxTargetInfo(Vector2Int center) {
            this.Center = center;
            this.GenerateTargets();
        }

        void GenerateTargets() {


            var hitPositions = new Vector2Int[4];
            int i = 0;
            for (int x = 0; x < 2; x++) {
                for (int y = 0; y < 2; y++) {
                    hitPositions[i] = new Vector2Int(this.Center.x + x, this.Center.y);
                    i++;
                }
            }
            this.Targets = hitPositions;
        }

    }


}
