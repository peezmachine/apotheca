﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Meta targeters are targeters that infer their target from other targets
 * and should THUS EXIST AS ORPHANS in the data, existing neither as a top-level
 * targeter nor as a subtargeter of a targeter group. They typically exist to
 * make things easier for comparison Requisites or in cases where we want the user
 * to make a user-friendly targeting choice that we want to treat a bit differently
 * in-logic (for example, the DirectionMetaTargeter is used when we want to ask
 * the user to pick a tile but want to infer a direction from that and another
 * targeter).
 * 
 * 
 */



//infers a direction from two positions
[CreateAssetMenu(menuName = "Apotheca/Targeter/Direction MetaTargeter")]
public class DirectionMetaTargeter :  MetaTargeter {
    public override TargeterType Type {
        get {
            return TargeterType.Direction;
        }
    }
    //public Vector2Int 

    [SerializeField] Targeter _fromPosition;
    [SerializeField] Targeter _toPosition;

    public override object GetTarget(IDictionary<TargeterBase, object> targets) {
        //return cached result if possible, otherwise nah
        Vector2Int from = (targets[_fromPosition] as TargetPackage<Vector2Int>).Target;
        Vector2Int to = (targets[_toPosition] as TargetPackage<Vector2Int>).Target;
        return new TargetPackage<Vector2Int>(to - from);
        //TODO -- normalize the result (write a v2int normalizer already)
    }
}
