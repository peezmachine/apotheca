﻿using System.Collections;
using System.Collections.Generic;
using Serializeable = System.SerializableAttribute;
using UnityEngine;



public enum TargeterType { Tile, Potion, TargeterGroup, Direction, Box }

/*
 * OK kids, we're going to go ahead and use ScriptableObject for data.
 * This will allow me to actually finish the project and easily configure
 * data in the Unity editor. However, it means the only way to add more
 * Targeters, Effects, or Apothecaries is to rebuild the project with the
 * new assets added. 
 * 
 * For future work, consider creating something like an ApothecaMakerTool
 * that displays data in visual editing environment and creates portable
 * JSON files. Then we just live that filename string life.
 * 
 * --> We can actually get it both ways -- build with SOs, and then at runtime, 
 * load custom JSON data and create runtime SOs -- we can't just can't save them 
 * to disk at this point, but they should work as holders for the data!
 * 
 * (we can always save modified SOs out as JSON)
 * 
 * 
 */


// Don't usually refer directly to ITargeter because targeters get 
//cast around a lot, and its easier if we can guarantee a reference type
//public interface ITargeter {

//    TargeterType Type { get; }

//    //string TargetDescription { get; }
//    //ICollection<IMetaTargeter> MetaTargeters { get; }
//}



public abstract class TargeterBase : ScriptableObject {
    public abstract TargeterType Type { get; }


}



public abstract class Targeter : TargeterBase  {
    
    
    public bool AutoTarget { get; protected set; }

    [SerializeField] string _targetDescription = "";
    [SerializeField] bool _effectsAreSequential = false; //if false, interpreters may try to lump for batches
    [SerializeField] List<MetaTargeter> _metaTargeters;
    [SerializeField] List<Requisite> _requisites = new List<Requisite>(4);
    [SerializeField] List<Effect> _effects = new List<Effect>(4);

  

    public List<Requisite> Requisites {
        get {
            return _requisites;
        }
    }

    public List<Effect> Effects {
        get {
            return _effects;
        }
    }

    public string TargetDescription {
        get {
            return _targetDescription;
        }
    }

    public bool EffectsAreSequential {
        get {
            return _effectsAreSequential;
        }
    }

    public ICollection<MetaTargeter> MetaTargeters {
        get {
            return _metaTargeters as ICollection<MetaTargeter>;
        }
    }
}

public abstract class Targeter<T> : Targeter {
    public TargetPackage<T> CreatePackage(T target) {
        return new TargetPackage<T>(target);
    }



}


//public interface IMetaTargeter : ITargeter  {
//    ITargetPackage GetTarget(IDictionary<ITargeter, ITargetPackage> targets); //uses target map to figure out its own targets 
//}

/*
 * Metatargeters are not "top level" targeters, and thus will never have requisites or effects attached to them.
 * (of course, reqs 'n' effects can reference metatargeters)
 * 
 * 
 */



public abstract class MetaTargeter : TargeterBase {
    public override abstract TargeterType Type { get; }

    /*
     * GetTarget has to take a targeting dictionary in order
     * to link its referenced targeters to their actual targets.
     */ 
    public abstract object GetTarget(IDictionary<TargeterBase, object> targets);

}

//public class IMetaTargeter<T> : MetaTargeter, ITargeter<T>

/*
 * So the deal with the whole target package thing. Target processing includes
 * a bit of type-casting, include of the target. It's easier and nicer to cast 
 * a reference type than a value type, so we deliver target data in the form
 * of a TargetPackage so we can just do "thingy as TargetPackage<T>" across the
 * board, instead of writing slightly different code if we expect a value type.
 * 
 * 
 */

public interface ITargetPackage<T>  {
    T Target { get; }
}

public interface  ITargetPackage {
      object Target { get; }

}

public class TargetPackage<T> : ITargetPackage, ITargetPackage<T> {
    public T Target { get; private set; }
    object ITargetPackage.Target { get { return this.Target as object; } }

    public TargetPackage(T target) {
        Target = target;
    }


    //TODO -- consider just overriding the equality comparer
    public bool IsDistinct(object other) {
        var specificOther = other as TargetPackage<T>;
        if (specificOther == null) {
            return true; // perhaps simplistic
        }

        return (this.Target.Equals(specificOther.Target));
    }
}





/*

 * 
 * --> NOTE: tried just making a thing that implements IDictionary
 * but the catch is that we really only want to expose the exteral
 * add/remove methods for proper Targeters, but the underlying data 
 * needs to accomodate TargeterBase, which includes meta-targeters
 * 
 * 
 * TargetingManager -- an object that provides functionality needed
 * to quickly and easy generate/remove meta-targeter targets when
 * their parent targer is fired.
 * 
 * 
 */
public class TargetingManager {

    public void AddTarget(Targeter targeter, object target, IDictionary<TargeterBase, object> targets) {
        targets[targeter] = target;
        foreach (MetaTargeter meta in targeter.MetaTargeters) {
            //use the target dictionary to get the meta targets


            targets[meta] = meta.GetTarget(targets);
        }
    }

    public void RemoveTarget(Targeter targeter, IDictionary<TargeterBase, object> targets) {
        targets.Remove(targeter);
        foreach (MetaTargeter meta in targeter.MetaTargeters) {
            targets.Remove(meta);
        }
    }




}



