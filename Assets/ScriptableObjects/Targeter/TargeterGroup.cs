﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Apotheca/Targeter/Targeter Group")]
public class TargeterGroup : Targeter {
    public override TargeterType Type { get { return TargeterType.TargeterGroup; } }

    [SerializeField] List<Targeter> _targeters;

    public List<Targeter> Targeters {
        get {
            return _targeters;
        }
    }

}
