﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Targeter/Tile Targeter")]
public class TileTargeter : Targeter<Vector2Int> {
    public override TargeterType Type { get { return TargeterType.Tile; } }

    [SerializeField] bool _mustBeEmpty = true;

    public bool MustBeEmpty {
        get {
            return _mustBeEmpty;
        }
    }
}