﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Targeter/Potion Targeter")]
public class PotionTargeter : Targeter<Vector2Int> {
    public override TargeterType Type { get { return TargeterType.Potion; } }

    public enum RevealedStatus { Either, Yes, No }
    [SerializeField] RevealedStatus _revealed = RevealedStatus.Either;

    public RevealedStatus Revealed {
        get {
            return _revealed;
        }
    }
}
