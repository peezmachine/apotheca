﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Targeter/Direction Targeter")]
public class DirectionTargeter : Targeter<Vector2Int> {
    public override TargeterType Type { get { return TargeterType.Direction; } }



    public enum DirectionChoice { North, East, South, West}

 



}
