﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Attach a requisite to a targeter and it will be checked as part of that targeter's validation.
 * NOTE: A requisite does not have to have ANYTHING to do with the targeter it is attached to --
 * the parent targeter only determines WHEN it will be checked.
 * 
 * 
 * 
 */ 

public abstract class Requisite : ScriptableObject {
    public enum RequisiteType { Distance, Position, ValidEffect, Distinctness }
    public abstract RequisiteType Type { get; }

}




