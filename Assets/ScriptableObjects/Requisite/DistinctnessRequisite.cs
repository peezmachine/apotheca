﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Apotheca/Requisite/Distinctness Requisite")]
public class DistinctnessRequisite : Requisite {

    public override RequisiteType Type {
        get {
            return RequisiteType.Distinctness;
        }
    }

    public List<TargeterBase> Entries {
        get {
            return _entries;
        }
    }

    [SerializeField] List<TargeterBase> _entries;


}
