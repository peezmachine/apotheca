﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Requisite/Distance Requisite")]
public class DistanceRequisite : Requisite {
    public override RequisiteType Type { get { return RequisiteType.Distance; } }

    public enum OrientationType { Any, Line, L}
    public enum DiagonalPermissionType { No, Yes, Only} 

    [SerializeField] OrientationType _orientation;
    [SerializeField] int _minDistance;
    [SerializeField] int _maxDistance;
    [SerializeField] DiagonalPermissionType _allowDiagonal; 
    [SerializeField] TargeterBase _origin;
    [SerializeField] TargeterBase _destination;


    //For other projects, probably smart to allow reqs to have multiple
    //entries. better to do one req twice than do two different req casts
    
    //public struct RequisiteInfo {
    //    public Targeter Origin;
    //    public Targeter Destination;

    //    public RequisiteInfo(Targeter origin, Targeter destination) {
    //        Origin = origin;
    //        Destination = destination;
    //    }
    //}



    public TargeterBase Origin {
        get {
            return _origin;
        }
    }

    public TargeterBase Destination {
        get {
            return _destination;
        }
    }

    public DiagonalPermissionType AllowDiagonal {
        get {
            return _allowDiagonal;
        }
    }

    public int MinDistance {
        get {
            return _minDistance;
        }

    }

    public int MaxDistance {
        get {
            return _maxDistance;
        }

    }

    public OrientationType Orientation {
        get {
            return _orientation;
        }
    }

  
}