﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * ValidEffectRequisite -- tells the target-checker to see if a given effect
 * would have any effect with the targets that have been chosen
 * 
 */


[CreateAssetMenu(menuName = "Apotheca/Requisite/Valid Effect Requisite")]
public class ValidEffectRequisite : Requisite {
    public override RequisiteType Type {
        get {
            return RequisiteType.ValidEffect;
        }
    }


    [SerializeField] List<Effect> _effects;

}
