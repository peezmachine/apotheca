﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Apotheca/Requisite/Position Requisite")]
public class PositionRequisite : Requisite {
    public override RequisiteType Type {
        get {
            return RequisiteType.Position;
        }
    }

    public EdgeAllowanceType EdgeAllowance {
        get {
            return _edgeAllowance;
        }

        set {
            _edgeAllowance = value;
        }
    }

    public Targeter Targeter {
        get {
            return _targeter;
        }
    }

    public enum EdgeAllowanceType { Either, Yes, No} // can the position be on the edge of the board or no
    [SerializeField] EdgeAllowanceType _edgeAllowance;
    [SerializeField] Targeter _targeter;  //TODO -- consider making this a list? 


}
