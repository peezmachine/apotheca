﻿using System.Collections;
using System.Collections.Generic;
using System;
using EzPz.Collections;


interface IState : IUpdateable {

    //bool IsActive { get; set; }
    void Enter();
    void Exit();
    void Pause();
    void Resume();
}




public abstract class BaseState : IState {

    public abstract void Update();
    
    protected bool IsActive { get; set; }

    public virtual void Enter() {
        this.IsActive = true;
    }

    public virtual void Pause() {
        this.IsActive = false;
    }

    public virtual void Resume() {
        this.IsActive = true;
    }

    //Exit called when the state is exited for any reason - push, tranition, or done
    /*
     * The state should not call Exit on itself -- instead, call Manager.Exit
     * 
     */ 
    public virtual void Exit() {
        this.IsActive = false;
    }

}


//TODO -- might want to add Suspend/Resume in addition to Enter/Exit


class StateManager {

    IState _currentState;
    NodeList<IState> _states = new NodeList<IState>(5);
    event System.Action _stackEmptySignal;


    public StateManager(System.Action stackEmptyCallback) {
        if (stackEmptyCallback != null) {
            _stackEmptySignal += stackEmptyCallback;
        }
    }



    public void Update() {
        _currentState.Update();
    }


    //Push -- provides a new current state, suspends the previous state on the stack
    public void Push(IState state) {
        if (_currentState != null) {
            _currentState.Pause();
            _states.Add(_currentState);
        }
        _currentState = state;
        _currentState.Enter();
    }

    //Transition -- replaces the current state with a new state, which is tossed aside
    public void Transition(IState newState) {
        if (_currentState != null)
            _currentState.Exit();
        _currentState = newState;
        _currentState.Enter();
    }


    //Pop -- discard the current state, resume the next state on the stack
    //  This is POST-EXIT of the current state. DO NOT CALL EXIT FROM HERE. 
     void Pop() {
        if (_states.Count > 0) {

            //the "pop"
            _currentState = _states.Tail;
            _states.Remove( _states.Tail);

            _currentState.Resume();
        } else { //stack is empty!
            _currentState = null;
            _stackEmptySignal.Invoke();
        }

    }

    public void Exit(IState state) {
        state.Exit();
        if (state == this._currentState) {
            this.Pop();
        } else {
            this._states.Remove(state);
        }



    }




}

//TODO -- maybe remove the distinction between Start and Resume, just call it Enter? 
/*
 * The Self-Managing State creates an ad-hoc stack of states without the need for an external manager.
 * Sending an Update call to a state will automatically propagate it to the states stacked above it.
 * 
 */ 
abstract class SelfManagingState : IUpdateable {


    event System.Action _onExit;
    event System.Action<SelfManagingState> _onTransitionOut;
    IUpdateable ForwardTarget; //something like a state that is pushed on top of this one

    protected bool IsActive = false;

    //XXX -- for now, leaving out the ability to spin up concurrent state machines like this. 
    //System.Action<IUpdateable> _branchAction; //the action to be called when a new independent state is created (not pushed/transitioned to) 

    public event System.Action OnExitSignal { add { _onExit += value; } remove { _onExit -= value; } }
    public event System.Action<SelfManagingState> OnTransitionSignal { add { _onTransitionOut += value; } remove { _onTransitionOut -= value; } }


    protected void PushState(SelfManagingState state) {
        this.Pause();
        this.ForwardTarget = state;
        state.OnExitSignal += this.Enter;
        state.Enter();
    }

    protected void TransitionToState(SelfManagingState state) {
        //when transitioning to some NewState from OldState, anyone who wanted to know when OldState was done
        // (so they could Resume) will instead be notified by NewState, which has replaced OldState on the ad-hoc stack
        state._onExit = this._onExit;
        this.Pause();
        this._onTransitionOut.Invoke(state);
        state.Enter();
    }

    public virtual void Update() {
        if (!this.IsActive) {
            if (this.ForwardTarget != null)
                this.ForwardTarget.Update();

            return;
        }
    }

    public virtual void Enter() { this.IsActive = true; }
    void Pause() { this.IsActive = false; }

    protected void Exit() {
        this.IsActive = false;
        _onExit.Invoke();
    }


}



public interface IUpdateable {
    void Update();
}

public interface IUpdateable<TUpdateArgs> {

    void Update(TUpdateArgs arg);
}


namespace Rad {

    public class Manager {

        SelfManagingState Target;




        public void Push(SelfManagingState state) {

        }
    }


    public abstract class SelfManagingState : IUpdateable {


        event System.Action _onExit;
        event System.Action<SelfManagingState> _onTransitionOut;
        event System.Action<SelfManagingState> _onPush;


        protected bool IsActive = false;

        //XXX -- for now, leaving out the ability to spin up concurrent state machines like this. 
        //System.Action<IUpdateable> _branchAction; //the action to be called when a new independent state is created (not pushed/transitioned to) 

        public event System.Action OnExitSignal { add { _onExit += value; } remove { _onExit -= value; } }
        public event System.Action<SelfManagingState> OnTransitionSignal { add { _onTransitionOut += value; } remove { _onTransitionOut -= value; } }
        public event System.Action<SelfManagingState> OnPushSignal { add { _onPush += value; } remove { _onPush -= value; } }

        protected void PushState(SelfManagingState state) {
            this.Pause();
            state.OnExitSignal += this.Enter;
            state.OnTransitionSignal += this.HandleTransition;
            _onPush.Invoke(state);
            state.Enter();
        }

        protected void TransitionToState(SelfManagingState state) {
            //when transitioning to some NewState from OldState, anyone who wanted to know when OldState was done
            // (so they could Resume) will instead be notified by NewState, which has replaced OldState on the ad-hoc stack
            this.Pause();
            this._onTransitionOut.Invoke(state);
            state.Enter();
        }

        public abstract void Update();

        // this is what gets called when a state which is pushed on top of this transitions to a new state
        void HandleTransition(SelfManagingState state) {
            state.OnExitSignal += this.Enter;
            state.OnTransitionSignal += HandleTransition;
        }


        public virtual void Enter() { this.IsActive = true; }
        void Pause() { this.IsActive = false; }

        protected void Exit() {
            this.IsActive = false;
            _onExit.Invoke();
        }


    }

}



//namespace Cool {

//    public class Manager {

//        SelfManagingState Target;




//        public void Push(SelfManagingState state) {
//            SelfManagingState oldTarget = this.Target;
//            state.OnExitSignal += delegate { this.Target = oldTarget; };
//            state.OnPushSignal += delegate { this.t}



//            state.OnTransitionSignal += HandleTransition;
//        }




//        void HandleTransition(SelfManagingState state) {
//            SelfManagingState oldTarget = this.Target;
//            state.OnExitSignal += oldTarget.Enter;

//            state.OnTransitionSignal += HandleTransition;
//        }

//    }


//    public abstract class SelfManagingState : IUpdateable {


//        event System.Action _onExit;
//        event System.Action<SelfManagingState> _onTransitionOut;
//        event System.Action<SelfManagingState> _onPush;


//        protected bool IsActive = false;

//        //XXX -- for now, leaving out the ability to spin up concurrent state machines like this. 
//        //System.Action<IUpdateable> _branchAction; //the action to be called when a new independent state is created (not pushed/transitioned to) 

//        public event System.Action OnExitSignal { add { _onExit += value; } remove { _onExit -= value; } }
//        public event System.Action<SelfManagingState> OnTransitionSignal { add { _onTransitionOut += value; } remove { _onTransitionOut -= value; } }
//        public event System.Action<SelfManagingState> OnPushSignal { add { _onPush += value; } remove { _onPush -= value; } }

//        protected void PushState(SelfManagingState state) {
//            this.Pause();
//            state.OnExitSignal += this.Enter;
//            state.OnTransitionSignal += this.HandleTransition;
//            _onPush.Invoke(state);
//            state.Enter();
//        }

//        protected void TransitionToState(SelfManagingState state) {
//            //when transitioning to some NewState from OldState, anyone who wanted to know when OldState was done
//            // (so they could Resume) will instead be notified by NewState, which has replaced OldState on the ad-hoc stack
//            this.Pause();
//            this._onTransitionOut.Invoke(state);
//            state.Enter();
//        }

//        public abstract void Update();

//        // this is what gets called when a state which is pushed on top of this transitions to a new state
//        void HandleTransition(SelfManagingState state) {
//            state.OnExitSignal += this.Enter;
//            state.OnTransitionSignal += HandleTransition;
//        }


//        public virtual void Enter() { this.IsActive = true; }
//        void Pause() { this.IsActive = false; }

//        protected void Exit() {
//            this.IsActive = false;
//            _onExit.Invoke();
//        }


//    }

//}


