﻿using UnityEditor;
using UnityEngine;
using System.IO;

public static class EditorUtility {

    /*
     * CreateAsset method courtesy of:
     * http://wiki.unity3d.com/index.php?title=CreateScriptableObjectAsset
     * Feb 12, 2017
     * 
     */
    public static T CreateAsset<T>(string path = "", string name = "") where T : ScriptableObject {
        T asset = ScriptableObject.CreateInstance<T>();

        if (path == "")
            path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "") { //rechecking in case we couldn't get a path
            path = "Assets";
        } else if (Path.GetExtension(path) != "") {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        if (name == "") {
            name = "/New " + typeof(T).ToString() + ".asset";
        } else {
            name = name + ".asset";
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + name);

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        UnityEditor.EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

        return asset;
    }
}