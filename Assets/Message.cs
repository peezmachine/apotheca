﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Messaging;
using EzPz.Collections;
using System;


public class ValidationError {
    //public enum ErrorType { None, SupplyEmpty}

    //ErrorType _type = ErrorType.None;

    //public ErrorType Type { get { return _type; } protected set { _type = value; } }

    public string Message;

    public ValidationError(string message = "Error") {
        this.Message = message;
    }

}


#region messages
public class GemsChangedMessage : Message {


    GameObject _target;
    int _oldAmount;
    int _newAmount;



    public GameObject Target {
        get {
            return _target;
        }
    }

    public int OldAmount {
        get {
            return _oldAmount;
        }
    }

    public int NewAmount {
        get {
            return _newAmount;
        }
    }

    public GemsChangedMessage(Message parent, GameObject target, int oldAmount, int newAmount) : base (parent) {
        _target = target;
        _oldAmount = oldAmount;
        _newAmount = newAmount;
    }
}

public class PotionRevealedMessage : Message {

    public Vector2Int Position;

    public PotionRevealedMessage(Message parent, Vector2Int position) : base(parent) {
        this.Position = position;
    }
}

public class PotionsMatchedMessage : Message {


    

    //public Potion LingeringPotion; // used for SP matches where a potion stays on the board
    public ICollection<Vector2Int> Positions;

    public PotionsMatchedMessage(Message parent,  ICollection<Vector2Int> positions) : base(parent) {
        this.Positions = positions;
    }
}

public class PotionsAddedMessage : Message {
    public ICollection<Vector2Int> Positions;

    public PotionsAddedMessage(Message parent, Action<Message> callback, params Vector2Int[] positions ) : base(parent) {
        this.Positions = positions;
    }
}

public class PotionsRemovedMessage : Message {

    public ICollection<Info> Removals;

    public PotionsRemovedMessage(Message parent, Action<Message> callback, params Info[] removals) : base(parent) {
        Removals = removals;
    }

    public struct Info {
        public Vector2Int Position;
        public Potion Potion;

        public Info(Vector2Int position, Potion potion) {
            Position = position;
            Potion = potion;
        }
    }

}

public class PotionsMovedMessage : Message {

    //TODO -- make this a readonly list instead
    public ICollection<PotionMovementInfo> Movements { get; private set; }

    public PotionsMovedMessage(Message parent, params PotionMovementInfo[] movements) : base(parent) {
        this.Movements = movements;
    }

    //TODO -- consider refactoring into a GridMovementInfo<T>
    public struct PotionMovementInfo {


        public Vector2Int FromPosition { get; private set; }
        public Vector2Int ToPosition { get; private set; }

        public Potion Potion { get; private set; }

        public PotionMovementInfo(Vector2Int fromPosition, Vector2Int toPosition, Potion potion) {
            FromPosition = fromPosition;
            ToPosition = toPosition;
            Potion = potion;
        }
    }
}

public class PlayerActionTakenMessage : Message {
    public PlayerActionTakenMessage(Message parent) : base(parent) {
    }
}

public class TurnEndedMessage : Message {
    public TurnEndedMessage(Message parent) : base(parent) {
    }
}

public class GameOverMessage : Message {
    public GameOverMessage(Message parent) : base(parent) {
    }
}
#endregion




#region commands
public class Command : Message {
    public bool ValidateOnly { get; private set; }
    public List<ValidationError> Errors;

    public Command(Message parent,  bool validateOnly = false) : base(parent) {
        this.ValidateOnly = validateOnly;
        this.Errors = new List<ValidationError>();
    }
}

public class ValidateTargeterCommand : Command {
    public Targeter Targeter;
    public IDictionary<TargeterBase, object> Targets; //values should be cast to assorted TargetPackage<T> 

    public ValidateTargeterCommand(Message parent,  Targeter targeter, IDictionary<TargeterBase, object> targets) : base(parent,  true) {
        this.Targeter = targeter;
        this.Targets = targets;
    }
}


//for SP matches where a potion stays on the board
public class ResolveMatchCommand : Command {

    public PotionsMatchedMessage MatchMessage;
    public Vector2Int PositionToKeep;
    

    public ResolveMatchCommand(Message parent,  PotionsMatchedMessage matchMessage, Vector2Int positionToKeep) :base(parent) {
        MatchMessage = matchMessage;
        PositionToKeep = positionToKeep;
    }
}

// the actual "place these potions at these places" command
public class RestockCommand : Command {
    public  Vector2Int[] Restocks;

    public RestockCommand(Message parent, params Vector2Int[] restocks) : base(parent) {
        Restocks = restocks;
    }
}




//Player Action Commands

class PlayerActionCommand : Command {
    public PlayerActionCommand(Message parent) : base(parent) {
    }
}





class RevealActionCommand : PlayerActionCommand {
    public Vector2Int Position;

    public RevealActionCommand(Message parent, Vector2Int position) : base(parent) {
        Position = position;
    }
}

class ApothecaryActionCommand : PlayerActionCommand {
    public Apothecary Apothecary;
    public IDictionary<TargeterBase, object> Targets;

    public ApothecaryActionCommand(Message parent, Apothecary apothecary, IDictionary<TargeterBase, object> targets) :base(parent) {
        Apothecary = apothecary;
        Targets = targets;
    }
}

//this is just a command saying "I want to take this action" -- seperate command to specify the potions/positions
class RestockActionCommand : PlayerActionCommand {
    public RestockActionCommand(Message parent) : base(parent) {
    }
}

class HireApothecaryActionCommand : PlayerActionCommand {
    public Potion.PotionColor Color { get; private set; } //which alley slot are we buying from
    public bool SingleColorPayment { get; private set; } //false means doing one-of-each
    public bool HireFromDeck { get; private set; } //true means ignore the color field

    //buying from deck
    public HireApothecaryActionCommand(Message parent) : base(parent) {
        this.SingleColorPayment = false;
        this.HireFromDeck = true;
    }

    //buying from colored slot
    public HireApothecaryActionCommand(Message parent,  Potion.PotionColor color, bool singleColorPayment) : base(parent){
        Color = color;
        SingleColorPayment = singleColorPayment;
        HireFromDeck = false;
    }

    //TODO -- could add a fully configurable constructor here and let the game logic
    // worry about which field combos make sense

}

class SatisfyApothecaryCommand : Command {

    public Apothecary Apothecary { get; private set; }


    public SatisfyApothecaryCommand(Message parent,  Apothecary apothecary) :base(parent) {
        this.Apothecary = apothecary;
    }

   
    
}




#endregion


#region requests
/*
 * Requests are messages typically sent by game logic asking the UI/AI to address some sort of prompt.
 * This prevents UI from having to know about game rules when creating prompts in response to game state changes.
 * For example, wihtout Requests, when potions are matched in a single-player game, the UI would have to know
 * to prompt the player for a restock position.
 * 
 * With Requests, the logic simply enqueues a RestockRequest (or whatever we call it) after the TurnFinished message.
 * The UI (or AI!) catches the request and acts accordingly.
 * 
 * 
 */


// Request for where to restock in a single-player game
public class RestockRequest : Message {
    public int Count;

    public RestockRequest(Message parent,  int count) :base(parent) {
        Count = count;
    }
}


public class ResolveMatchRequest : Message {
    public PotionsMatchedMessage MatchMessage;
    //TODO -- consider paring this down to just the parent arg, and having a get property just return the tail
    public ResolveMatchRequest(Message parent, PotionsMatchedMessage matchMessage) :base(parent) {
        MatchMessage = matchMessage;
    }
}

public class SatisfyApothecaryRequest : Message {
    public SatisfyApothecaryRequest(Message parent) : base(parent) {
    }
}




#endregion

#region errors



public class RestockUnavailableError : ValidationError {
    public enum ErrorType {SupplyEmpty, TilesBlocked}

    public ErrorType Type { get; private set; }

    public RestockUnavailableError(ErrorType type ) : base() {
        this.Type = type;

        switch (type) {
            case ErrorType.SupplyEmpty:
                this.Message = "supply is empty";
                break;
            case ErrorType.TilesBlocked:
                this.Message = "ell restock tiles are blocked";
                break;
            default:
                this.Message = "restock unavailable";
                break;
        }
    }

}



#endregion