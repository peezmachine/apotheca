﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

//TODO -- remove the Unity dependencies, use System.Random



public static class Utility {

    public delegate void VoidEventHandler();

    public static List<T> RemoveItemsFromList<T>(IList<T> items, IList<int> indices) {
        // sort the index list, reverse it, then remove that shniz
        Debug.Log("Utility.RemoveItemsFromList " + indices.ToString());
        List<int> _indices = new List<int>(indices);
        _indices.Sort();
        _indices.Reverse();
        List<T> removedItems = new List<T>(indices.Count);
        foreach (int index in _indices) {
            removedItems.Add(items[index]);
            items.RemoveAt(index);
        }

        return removedItems;
    }



    public static void Shuffle<T>(this IList<T> items) {
        int count = items.Count;
       
        for (int i = 0; i < (count-2); i++) { //the 2 because we don't need last item to swap with self.
            int swapIndex = UnityEngine.Random.Range(i, count);
            T swapItem = items[swapIndex];
            items[swapIndex] = items[i];
            items[i] = swapItem;
        }
    }
    /*
	public static void AddRange<T>(this ObservableCollection<T> c, IList<T> items){
		foreach (T item in items)
			c.Add (item);
	}
	*/

    public static T GetOrAddComponent<T>(this GameObject go) where T : Component {
        T result = go.GetComponent<T>();
        if (result != null)
            return result;
        return go.AddComponent<T>();
    }

    public static T GetOrAddComponent<T>(this Component c) where T : Component {
        return c.gameObject.GetOrAddComponent<T>();
    }

    //Note: don't repeatedly call GetRandomElement to get multiple elements, since it must make/kill the copy each time
    // Instead, use GetRandomElements!
    public static T GetRandomElement<T>(this HashSet<T> items) {
        int size = items.Count;
        T[] itemArray = new T[size];
        int index = (int)UnityEngine.Random.Range(0, size);
        return itemArray[index];
    }



    public static List<T> GetRandomElements<T>(this HashSet<T> items, int count, bool distinct = true) {
        if (distinct) {
            List<T> itemsList = new List<T>(items);
            itemsList.Shuffle();
            return itemsList.GetRange(0, count);
        }

        int size = items.Count;
        T[] itemArray = new T[size];
        items.CopyTo(itemArray);
        List<T> results = new List<T>(count);


        for (int i = 1; i <= count; i++) {
            int index = UnityEngine.Random.Range(0, size);
            results.Add(itemArray[index]);
        }
        return results;
    }



    public static IEnumerable<Type> GetInheritanceTypes(this Type type) {
        for (var current = type; current != null; current = current.BaseType)
            yield return current;

    }


    //A method that can used as a message subscriber to automate null-checking 
    /* example:  
     * messageManager.Subscribe(delegate (BaseThing m) {ExecuteIfPossible<DerivedThing>(CoolMethod, m)});
     * 
     * 
     * 
     * 
     */ 
    public static void ExecuteIfPossible<T>(Action<T> a, object b) where T : class {
        T message = b as T;
        if(message != null) {
            a.Invoke(message);
        }

    }


}
