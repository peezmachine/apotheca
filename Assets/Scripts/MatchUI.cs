﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RotaryHeart.Lib.SerializableDictionary;
using EzPz.Collections.Observable;
using UnityEngine.Events;
using EzPz.Collections;
using EzPz.Messaging;

public class MatchUI : MonoBehaviour {

    #region fields
    public SolitaireMatchModel MatchModel;


    GameObject _potionTilePrefab;
    GameObject _boardTilePrefab;
    public ColorTextDictionary GemInfo;
    Dictionary<Potion, GameObject> PotionViewEntities = new Dictionary<Potion, GameObject>();
    Dictionary<Potion, GameObject> RestockPotions = new Dictionary<Potion, GameObject>();

    public GameObject ApothecaryPanel;
    Dictionary<Apothecary, ApotehecaryEntry> _apothecaryEntries;

    StateManager StateManager;

    GameObject ButtonPrefab;

    [SerializeField] Canvas _worldCanvas; //for placing some ui things relative to the board

    [SerializeField] Text _userPromptText;

    [SerializeField] Text _movesLeftText;

    [SerializeField] GameObject _restockMarkerPrefab;

    [SerializeField] GameObject _interTileBoxPrefab;

    [SerializeField] GameObject _directionButtonPrefab; 

    [SerializeField] Button _toggleRestockButton;


    [SerializeField] SceneInitializer _sceneInitializer;



    //[SerializeField] Game _game; //TODO -- remove this, only here to simplify target-validiation testing


    Dictionary<Potion.PotionColor, Color> PotionColorMap = new Dictionary<Potion.PotionColor, Color>() {
            { Potion.PotionColor.Red , Color.red },
            { Potion.PotionColor.Blue , Color.blue },
            { Potion.PotionColor.Yellow , Color.yellow }
        };

    [SerializeField] ColorButtonDictionary _apothecaryAlleyButtons;


    [System.Serializable]
    public class ColorTextDictionary : SerializableDictionaryBase<Potion.PotionColor, Text> { }
    #endregion


    #region properties
    MessageProcessor MessageProcessor { get; set; }
    SubscriptionBroker SubscriptionBroker { get; set; }



    private Text UserPromptText {
        get {
            return _userPromptText;
        }

        set {
            _userPromptText = value;
        }
    }

    //public Game Game {
    //    get {
    //        return _game;
    //    }
    //}

    Dictionary<Vector2Int, SpriteRenderer> InterTileBoxes = new Dictionary<Vector2Int, SpriteRenderer>();

    Dictionary<DirectionTargeter.DirectionChoice, Button> DirectionButtons = new Dictionary<DirectionTargeter.DirectionChoice, Button>();
    #endregion


    [System.Serializable]
    class ColorButtonDictionary : SerializableDictionaryBase<Potion.PotionColor, Button> { }





    private void Awake() {
        _potionTilePrefab = Resources.Load<GameObject>("Prefab/PotionTile");
        _boardTilePrefab = Resources.Load<GameObject>("Prefab/BoardTile");
        this.ButtonPrefab = Resources.Load<GameObject>("Prefab/UI/Unity/Button");
        this.StateManager = new StateManager(null);

       
    }


    void Start() {
        this.MessageProcessor = _sceneInitializer.MessageManagerProvider.Processor;
        this.SubscriptionBroker = _sceneInitializer.MessageManagerProvider.BrokerProvider.Broker;
        


        #region apothecary panel setup
        GameObject entryPrefab = Resources.Load<GameObject>("Prefab/UI/MatchUI/ApothecaryEntry");
        _apothecaryEntries = new Dictionary<Apothecary, ApotehecaryEntry>(6);
        //set up apothecary panel
        //XXX -- this is hacky, clearing the children
        foreach (Transform child in ApothecaryPanel.transform) {
            Destroy(child.gameObject);
        }
        #endregion


        //create (and disable for now) the direction buttons
        Dictionary<DirectionTargeter.DirectionChoice, Vector2Int> directions = new Dictionary<DirectionTargeter.DirectionChoice, Vector2Int>() {
            {DirectionTargeter.DirectionChoice.North, Vector2Int.up },
            {DirectionTargeter.DirectionChoice.East, Vector2Int.right },
            {DirectionTargeter.DirectionChoice.South, Vector2Int.down },
            {DirectionTargeter.DirectionChoice.West, Vector2Int.left }

        };

        foreach (KeyValuePair<DirectionTargeter.DirectionChoice, Vector2Int> kvp in directions) {
            Button button = GameObject.Instantiate(this._directionButtonPrefab).GetComponent<Button>();
            button.transform.SetParent(this._worldCanvas.transform);
            button.transform.position = new Vector3(-.5f + 2.5f * kvp.Value.x, -.5f + 2.5f * kvp.Value.y, 0);
            button.gameObject.SetActive(false);
            this.DirectionButtons[kvp.Key] = button;
        }



        #region event subscribe





            //this.MatchModel.PotionsChangedNotifier.OnCollectionChanged += delegate (object sender, CollectionChangedEventArgs<KeyValuePair<Vector2Int, Potion>> args) {
            //    this.OnPotionsChanged(args);
            //};


            #endregion




            //set the gem info, observe changes
            foreach (Potion.PotionColor color in MatchModel.Player.Gems.Keys) {
            this.UpdateGemInfo(color, MatchModel.Player.Gems[color]);
            MatchModel.Player.Gems.OnCollectionChanged += delegate (System.Object sender, CollectionChangedEventArgs<KeyValuePair<Potion.PotionColor, int>> e) {
                switch (e.Type) {
                    case CollectionChangedEventArgs.ChangeEventType.Replace:
                        this.UpdateGemInfo(e.NewItems[0].Key, e.NewItems[0].Value);
                        break;
                    default:
                        break;

                }

            };

        }


        //draw the board 
        float startX = -2;
        float startY = -2;

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                var position = new Vector3(startX + x, startY + y, 0);
                var cellPosition = new Vector2Int(x, y);

                //1) Make the tile
                GameObject tileEntity = GameObject.Instantiate(_boardTilePrefab);
                tileEntity.transform.position = position;
                CellTransform tileCellTransform = tileEntity.GetOrAddComponent<CellTransform>();
                tileCellTransform.Position = cellPosition;



                //2) make the potion, if needed
                if (MatchModel.Potions.ContainsKey(cellPosition))
                    this.CreatePotionViewEntity(cellPosition);


                //3 make the restock potion
                //TODO -- maybe make these the actual potion views, just move them on restock (instead of kill/make as it is now)
                GameObject potionEntity = GameObject.Instantiate(_potionTilePrefab);
                potionEntity.GetComponent<SpriteRenderer>().color = Color.black;
                int bonusX = -3;
                if (x > 1)
                    bonusX += 6;
                potionEntity.transform.position = new Vector3(startX + x + bonusX, startY + y, 0);

                this.RestockPotions[MatchModel.Supply[cellPosition]] = potionEntity;

            }
        }

        startX += .5f;
        startY += .5f;
        //add the inter-tile colliders for box targeting
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                

                var position = new Vector3(startX + x, startY + y, 0);
                var cellPosition = new Vector2Int(x, y);

                //1) Make the tile
                GameObject tileEntity = GameObject.Instantiate(_interTileBoxPrefab);
                tileEntity.transform.position = position;
                CellTransform tileCellTransform = tileEntity.GetOrAddComponent<CellTransform>();
                tileCellTransform.Position = cellPosition;
                this.InterTileBoxes[cellPosition] = tileEntity.GetComponent<SpriteRenderer>();
                tileEntity.SetActive(false); // don't need them active in the base state
            }
        }




        MainState state = new MainState(this);
        this.StateManager.Transition(state);

        

    }


    private void CreatePotionViewEntity(Vector2Int boardPosition) {
        Vector3 position = new Vector3(-2 + boardPosition.x, -2 + boardPosition.y, 0);
        Potion potion = MatchModel.Potions[boardPosition];

        GameObject potionEntity = GameObject.Instantiate(_potionTilePrefab);
        potionEntity.transform.position = position + new Vector3(0, 0, -1);
        CellTransform potionCellTransform = potionEntity.GetOrAddComponent<CellTransform>();
        potionCellTransform.Position = boardPosition;


        SpriteRenderer renderer = potionEntity.GetComponent<SpriteRenderer>();
        renderer.sortingOrder = 5;
        if (potion.IsRevealed) {
            renderer.color = this.PotionColorMap[potion.Color];
        } else {
            renderer.color = Color.black;
            //check peekability
            if (potion.Owner != null && potion.Owner == MatchModel.Player) {
                //XXX TODO -- don't look up by GOs, use components with refs
                GameObject hintEntity = potionEntity.transform.GetChild(0).gameObject;
                hintEntity.GetComponent<SpriteRenderer>().color = this.PotionColorMap[potion.Color];
                hintEntity.SetActive(true);
            }

        }




        this.PotionViewEntities[potion] = potionEntity;


    }

    private void Update() {
        this.StateManager.Update();
    }

    void OnPotionsChanged(CollectionChangedEventArgs<KeyValuePair<Vector2Int, Potion>> change) {

    }

    void UpdateGemInfo(Potion.PotionColor color, int count) {
        GemInfo[color].text = color.ToString() + " " + count;

    }


    #region coroutine state machine thing
    //IEnumerator BaseState() {
    //    Debug.Log("started base UI");
    //    GameObject clickedObject = null;
    //    Ray ray;
    //    RaycastHit2D hit;


    //    bool doTransition = false;
    //    System.Func<StateResumeToken> transitionAction = null;
    //    //StateResumeToken transitionToken = null;


    //    System.Func<System.Func<StateResumeToken, IEnumerator>, StateResumeToken> transitionToState = delegate (System.Func<StateResumeToken, IEnumerator> a) {
    //        StateResumeToken transitionToken = new StateResumeToken();
    //        StartCoroutine(a(transitionToken));
    //        return transitionToken;
    //    };



    //    System.Func<StateResumeToken> goToKingState = delegate () {
    //        StateResumeToken transitionToken = new StateResumeToken();
    //        StartCoroutine(KingMove(transitionToken));
    //        return new StateResumeToken();
    //    };








    //    //set up listeners for UI events of interest
    //        foreach (ApotehecaryEntry entry in _apothecaryEntries.Values) {
    //        entry.Button.onClick.AddListener(delegate () {
    //            switch (entry.Apothecary.Ability.Type) {
    //                case Ability.AbilityType.WanderingWaltz:
    //                    doTransition = true;
    //                    transitionAction = delegate () {
    //                        return transitionToState(KingMove);
    //                    };
    //                    break;
    //                default:
    //                    break;
    //            }
    //        });
    //    }




    //    while (true) {

    //        if (doTransition) {
    //            yield return transitionAction.Invoke();
    //        }


    //        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //        hit = Physics2D.Raycast(ray.origin, ray.direction);
    //        if (hit != default(RaycastHit2D)) {  // we have a raycast hit
    //            if (Input.GetMouseButtonDown(0)) { //getting the mouse-down part of the click
    //                Debug.Log("clicking something");
    //                clickedObject = hit.collider.gameObject;
    //            } else if (Input.GetMouseButtonUp(0)) {  //getting mouse-up part of click
    //                if (hit.collider.gameObject == clickedObject) {
    //                    Debug.Log("got a full up-down click!");
    //                    //we've got a full click, let's flip it if it's a hidden potion
    //                    if (hit.collider.gameObject.CompareTag("Potion")) {
    //                        Debug.Log("and it's a potion!");
    //                        CellPosition cellPosition = hit.collider.GetComponent<CellPosition>();
    //                        Potion potion = MatchModel.BoardTiles[cellPosition.X, cellPosition.Y];
    //                        if (!potion.IsRevealed) {
    //                            this.Game.RevealPotion(cellPosition.X, cellPosition.Y);
    //                        }
    //                    } else {  //not a proper click, reset it!
    //                        clickedObject = null;
    //                    }
    //                }

    //            }

    //        }


    //        if (Input.GetKeyDown(KeyCode.T)) {
    //            Debug.Log("pressed T");

    //            StateResumeToken resumeToken = new StateResumeToken();
    //            StartCoroutine(Target(resumeToken));
    //            yield return resumeToken;
    //        }


    //        if (Input.GetKeyDown(KeyCode.K)) {
    //            Debug.Log("pressed K");

    //            StateResumeToken resumeToken = new StateResumeToken();
    //            StartCoroutine(KingMove(resumeToken));
    //            yield return resumeToken;
    //        }



    //        yield return null;
    //    }
    //}



    //IEnumerator Target(StateResumeToken exitToken) {
    //    //really dumbo test thing here, just want to go into a "select a row" mode and finish up once the player has clicked one

    //    bool haveTarget = false;
    //    int mask = LayerMask.GetMask("Board");

    //    CellPosition clickedPosition = null;
    //    Ray ray;
    //    RaycastHit2D hit;


    //    while (true) {
    //        //2D raycasting
    //        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //        hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, mask);

    //        if (hit != default(RaycastHit2D)) {
    //            if (Input.GetMouseButtonDown(0)) {
    //                clickedPosition = hit.collider.GetComponent<CellPosition>();

    //            } else if (Input.GetMouseButtonUp(0) && clickedPosition != null) {
    //                CellPosition upPosition = hit.collider.GetComponent<CellPosition>();
    //                if (upPosition.Y == clickedPosition.Y) {
    //                    Game.MoveRow(clickedPosition.Y);
    //                    break;
    //                } else { //didn't click on the same row, so abort and re-target
    //                    clickedPosition = null;
    //                }
    //            }
    //        }
    //        yield return null;
    //    }


    //    exitToken.SetKeepWaiting(false);
    //    yield break;
    //}






    //IEnumerator KingMove(StateResumeToken exitToken) {

    //    bool haveSource = false;

    //    int mask = LayerMask.GetMask("Board");
    //    CellPosition source = null;
    //    CellPosition destination = null;
    //    GameObject clickedObject = null;
    //    Ray ray;
    //    RaycastHit2D hit;

    //    while (true) {
    //        //need to make sure the source we get is a card
    //        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

    //        if (haveSource) {
    //            hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, mask); // just gets tiles
    //        } else {
    //            hit = Physics2D.Raycast(ray.origin, ray.direction); //TODO -- target potions specifically
    //        }



    //        if (hit != default(RaycastHit2D)) {
    //            if (Input.GetMouseButtonDown(0) && clickedObject == null) {
    //                clickedObject = hit.collider.gameObject;



    //            } else if (Input.GetMouseButtonUp(0)) {
    //                if (hit.collider.gameObject == clickedObject) {
    //                    Debug.Log("good click");
    //                    if (haveSource) {

    //                        destination = hit.collider.GetComponent<CellPosition>();
    //                        if (destination.X != source.X || destination.Y != source.Y) { //got unique destination
    //                            Debug.Log("got destination tile");
    //                            break;
    //                        } else { //tried to move to source position, try it again
    //                            destination = null;
    //                        }
    //                    } else if (hit.collider.gameObject.CompareTag("Potion")) { //need source potion, got one
    //                        source = hit.collider.GetComponent<CellPosition>();
    //                        Debug.Log("got source potion");
    //                        haveSource = true;
    //                        clickedObject = null;
    //                    }

    //                }

    //                clickedObject = null; //reset clicked object on mouse up no matter what
    //            }



    //        }

    //        yield return null;
    //    }
    //    Debug.LogFormat("UI trying to move potion from {0},{1} to {2},{3}", source.X, source.Y, destination.X, destination.Y);
    //    Game.MovePotion(source.X, source.Y, destination.X, destination.Y);



    //    exitToken.SetKeepWaiting(false);
    //    yield break;
    //}


    //public class StateResumeToken : CustomYieldInstruction {

    //    bool _keepWaiting = true;

    //    public override bool keepWaiting {
    //        get {
    //            return _keepWaiting;
    //        }
    //    }

    //    //since we can't override/define keepWaiting.set
    //    public void SetKeepWaiting(bool value) {
    //        _keepWaiting = value;
    //    }

    //}
    #endregion









    class ApotehecaryEntry {
        public Apothecary Apothecary;
        public Button Button;

        public ApotehecaryEntry(Apothecary apothecary, Button button) {
            Apothecary = apothecary;
            Button = button;
        }
    }


    abstract class State : BaseState {

        //push = go to new state but push current to stack; transition = go to new state and destroy current one
        //protected enum StateAction {Push, Transition}

        protected MatchUI UI;



        public State(MatchUI ui) : base() {
            this.UI = ui;
        }

        protected StateManager Manager { get { return  this.UI.StateManager; } }

    }



    class MainState : State {

        Ray ray;
        RaycastHit2D hit;
        GameObject clickedObject;
        bool RestockGuideVisible = false;

        List<GameObject> RestockGuides = new List<GameObject>();

        public override void Enter() {
            base.Enter();
            this.UI.UserPromptText.text = "Just Playin'";
        }

        public override void Resume() {
            base.Resume();
            this.UI.UserPromptText.text = "Just Playin'";
        }

        void HireApothecary(Potion.PotionColor color) {
            if (!this.IsActive)
                return;

            //fire off a HireApothecary command
            var command = new HireApothecaryActionCommand(null, color, true); //XXX -- force true is solitaire-only
            this.UI.MessageProcessor.EnqueueMessage(command);
        }

        public MainState(MatchUI ui) : base(ui) {

            //set up turn info panel
            this.UI._movesLeftText.text = this.UI.MatchModel.MovesLeft.ToString();


            #region UI hooks
            //set up listeners for UI events of interest
            foreach (ApotehecaryEntry entry in this.UI._apothecaryEntries.Values) {
                //just spin up the ability-use state and let it handle it from there
                
                entry.Button.onClick.AddListener(delegate { if (this.IsActive) this.GoToAbilityState(entry.Apothecary); }); 

            }

            foreach(Potion.PotionColor color in System.Enum.GetValues(typeof(Potion.PotionColor))) {
                //TODO -- move all this button value stuff into prefabs for the ui
                Button button = this.UI._apothecaryAlleyButtons[color];
                Color baseColor = this.UI.PotionColorMap[color];
                Color imageColor = new Color(baseColor.r, baseColor.g, baseColor.b, .3f); //changing button alpha
                button.GetComponent<Image>().color = imageColor;
                button.onClick.AddListener(delegate { this.HireApothecary(color); });
            }

            this.UI._toggleRestockButton.onClick.AddListener(this.ToggleRestockGuide);
            #endregion

            #region event hook-in
            //potion revealed
            this.UI.SubscriptionBroker.Subscribe(key: typeof(PotionRevealedMessage), subscriber: delegate (Message m) {
                PotionRevealedMessage message = m as PotionRevealedMessage;
                Potion potion = this.UI.MatchModel.Potions[message.Position];
                 GameObject viewEntity = this.UI.PotionViewEntities[potion];
                viewEntity.GetComponent<SpriteRenderer>().color = this.UI.PotionColorMap[potion.Color];

                //remove peek hint
                //XXX TODO -- don't use hacky GO-based approach here
                viewEntity.transform.GetChild(0).gameObject.SetActive(false);

            });

            //potions moved
            this.UI.SubscriptionBroker.Subscribe(key: typeof(PotionsMovedMessage), subscriber: delegate (Message m) {
                PotionsMovedMessage message = m as PotionsMovedMessage;
                foreach (PotionsMovedMessage.PotionMovementInfo info in message.Movements) {
                    Potion potion = info.Potion;
                    GameObject tileEntity = this.UI.PotionViewEntities[potion];
                    CellTransform cellTransform = tileEntity.GetComponent<CellTransform>();
                    cellTransform.Position = info.ToPosition;
                    tileEntity.transform.position = new Vector3(-2 + cellTransform.Position.x, -2 + cellTransform.Position.y, 0); // XXX - very hacky, assumes width = 1 and start = -2
                }

            });

            //match resolve request
            this.UI.SubscriptionBroker.Subscribe(key: typeof(ResolveMatchRequest), subscriber: delegate (Message m) {
                var message = m as ResolveMatchRequest;
                this.Manager.Push(new PlacePotionStackState(this.UI, message));
            });

            //satisfy apothecary request
            this.UI.SubscriptionBroker.Subscribe(key: typeof(SatisfyApothecaryRequest), subscriber: delegate (Message m) {
                //var message = m as ResolveMatchRequest;
                this.Manager.Push(new SatisfyApothecaryState(this.UI, m as SatisfyApothecaryRequest));
            });


            //potions added
            this.UI.SubscriptionBroker.Subscribe(key: typeof(PotionsAddedMessage), subscriber: delegate (Message m) {
                var message = m as PotionsAddedMessage;
                foreach (Vector2Int position in message.Positions) {
                    this.UI.CreatePotionViewEntity(position);
                    Potion potion = ui.MatchModel.Potions[position];
                    GameObject entity = UI.RestockPotions[potion];


                    this.UI.RestockPotions.Remove(ui.MatchModel.Potions[position]); //removing restock view for the potion that was added, if we have it

                    //assuming the potion came from a restock for now. XXX TODO make this not dumb as hell                                                       
                    if (this.RestockGuideVisible) {
                        this.ToggleRestockGuide();
                        this.ToggleRestockGuide();
                    }
                    GameObject.Destroy(entity);
                }
            });


            //potions removed
            this.UI.SubscriptionBroker.Subscribe(key: typeof(PotionsRemovedMessage), subscriber: delegate (Message m) {
                var message = m as PotionsRemovedMessage;
                foreach (PotionsRemovedMessage.Info info in message.Removals) {
                    GameObject entity = this.UI.PotionViewEntities[info.Potion];
                    this.UI.PotionViewEntities.Remove(info.Potion);
                    GameObject.Destroy(entity);
                }
            });

            //game over
            this.UI.SubscriptionBroker.Subscribe(key: typeof(GameOverMessage), subscriber: delegate (Message m) {
                this.Manager.Transition(new GameOverState(this.UI ));
            });

            //restock request
            this.UI.SubscriptionBroker.Subscribe(key: typeof(RestockRequest), subscriber: delegate (Message m) {
                this.Manager.Push(new RestockState(this.UI, m as RestockRequest));
            });

            //player action taken
            this.UI.SubscriptionBroker.Subscribe(key: typeof(PlayerActionTakenMessage), subscriber: delegate (Message m) {
                this.UI._movesLeftText.text = this.UI.MatchModel.MovesLeft.ToString();
            });

            #endregion

            #region data observe
            ui.MatchModel.Player.Apothecaries.OnCollectionChanged += this.Apothecaries_OnCollectionChanged;
            ui.MatchModel.AlleyChangedNotifier.OnCollectionChanged += this.AlleyChangedNotifier_OnCollectionChanged;
            #endregion


        }

        private void AlleyChangedNotifier_OnCollectionChanged(object sender, CollectionChangedEventArgs<KeyValuePair<Potion.PotionColor, Apothecary>> e) {
            //if change or add, set the corresponding button visuals (text)
            if (e.Type == CollectionChangedEventArgs.ChangeEventType.Add || e.Type == CollectionChangedEventArgs.ChangeEventType.Replace) {
                foreach ( KeyValuePair < Potion.PotionColor, Apothecary > kvp in e.NewItems) {
                    this.UI._apothecaryAlleyButtons[kvp.Key].transform.GetChild(0).GetComponent<Text>().text = kvp.Value.Name;
                    //TODO -- also include color in this text
                }
            }

        }

        //TODO -- make this a lot smarter than the brute-in slashburn-out that it is
        void ToggleRestockGuide() {
            if (this.RestockGuideVisible) {
                this.RestockGuideVisible = false;
                foreach (GameObject guide in this.RestockGuides) {
                    GameObject.Destroy(guide);
                    
                }
                this.RestockGuides.Clear();
            } else {
                foreach (Vector2Int boardPosition in this.UI.MatchModel.Supply.Keys) {
                    GameObject guide = GameObject.Instantiate(this.UI._restockMarkerPrefab);
                    guide.transform.position = new Vector3(-2 + boardPosition.x, -2 + boardPosition.y, 0);
                    guide.GetComponent<SpriteRenderer>().sortingOrder = 20;
                    this.RestockGuides.Add(guide);
                }
                this.RestockGuideVisible = true;
            }
        }

        private void AddApothecary(Apothecary apothecary) {
            //make a new button, put in the right UI element, listen for click to go to ability state
            GameObject buttonEntity = GameObject.Instantiate<GameObject>(this.UI.ButtonPrefab);
            buttonEntity.transform.SetParent(this.UI.ApothecaryPanel.transform);
            Text text = buttonEntity.GetComponentInChildren<Text>();
            buttonEntity.transform.SetParent(UI.ApothecaryPanel.transform);
            text.text = apothecary.Name;
            var button = buttonEntity.GetComponent<Button>();
            button.onClick.AddListener(delegate { if(this.IsActive) this.GoToAbilityState(apothecary); });
            this.UI._apothecaryEntries.Add(apothecary, new ApotehecaryEntry(apothecary, button));
        }



        private void Apothecaries_OnCollectionChanged(object sender, CollectionChangedEventArgs<Apothecary> e) {
            switch (e.Type) {
                case CollectionChangedEventArgs.ChangeEventType.Add:
                    foreach (Apothecary apothecary in e.NewItems) {
                        this.AddApothecary(apothecary);
                    }
                    break;
                case CollectionChangedEventArgs.ChangeEventType.Remove:
                    foreach (Apothecary apothecary in e.OldItems) {
                        ApotehecaryEntry entry = this.UI._apothecaryEntries[apothecary];
                        this.UI._apothecaryEntries.Remove(apothecary);
                        GameObject.Destroy(entry.Button.gameObject);
                    }
                    break;
                default:
                    throw new System.NotImplementedException();
            }
        }

        void GoToAbilityState(Apothecary apothecary) {
            UseApothecaryState state = new UseApothecaryState(this.UI, apothecary);
            this.Manager.Push(state);
        }




        public override void Update() {


            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit != default(RaycastHit2D)) {  // we have a raycast hit

                //TODO -- put "highlight retock poistions when mouse over supply" here

                if (Input.GetMouseButtonDown(0)) { //getting the mouse-down part of the click
                    clickedObject = hit.collider.gameObject;
                } if (Input.GetMouseButtonUp(0)) {  //getting mouse-up part of click
                    if (hit.collider.gameObject == clickedObject) {
                        //we've got a full click, let's flip it if it's a hidden potion
                        if (hit.collider.gameObject.CompareTag("Potion")) {
                            var cellTransform = hit.collider.GetComponent<CellTransform>();
                            Potion potion = this.UI.MatchModel.Potions[cellTransform.Position];
                            if (!potion.IsRevealed) {
                                this.UI.MessageProcessor.EnqueueMessage(new RevealActionCommand(null,  cellTransform.Position));
                            }
                        } else {  //not a proper click, reset it!
                            clickedObject = null;
                        }
                    }

                }

            } else if (Input.GetMouseButtonDown(0)) {
                this.clickedObject = null;
            }

        }
    }

    #region states
    class UseApothecaryState : State {

        Apothecary Apothecary;
        bool NeedTargeters = true;


        IDictionary<TargeterBase, object> Targets; // Targeter -> TargetPackage<T>

        public UseApothecaryState(MatchUI ui, Apothecary apothecary) : base(ui) {
            this.Targets = new Dictionary<TargeterBase, object>();
            this.Apothecary = apothecary;

        }


        /*
         * Get the apothecary's ability
         * when something is clicked on, see if it satisfies the targeter 
         * 
         * 
         * Right now, abilities only satisfy a single targeter so we only need to
         * push a single targeter-specific state. If we move to support multiple targeters
         * at the ground level, we would instead need to spin up a new stack (Scene Manager)
         * for each one and listen for exits
         * 
         */

        void Cool() {
            if (this.NeedTargeters) {
                this.NeedTargeters = false;
                this.DealWithTargeter(Apothecary.Ability.Targeter);
            } else { //we have targets, execute it
                var command = new ApothecaryActionCommand(null, this.Apothecary, this.Targets);
                command.OnFinish += delegate { this.Manager.Exit(this); };
                this.UI.MessageProcessor.EnqueueMessage(command);

                //this.Manager.Exit(this);
            }

        }

        //var ability = Apothecary.Ability;
        public override void Enter() {
            base.Enter();
            this.Cool();
        }

        public override void Resume() {
            base.Resume();
            this.Cool();
        }
        public override void Update() {
        }







        void DealWithTargeter(Targeter targeter) {
            switch (targeter.Type) {
                case TargeterType.TargeterGroup:
                    /*
                     * Need to get the first targeter, and then somehow cue off of
                     * that to get the 2nd targeter, etc.
                     * or just push all the states on in REVERSE order.
                     * so push T3, T2, and then T1, so they resolve in order. Cool?
                     */
                    List<Targeter> targeters = new List<Targeter>((targeter as TargeterGroup).Targeters);
                    targeters.Reverse();
                    foreach (Targeter t in targeters) {
                        this.DealWithTargeter(t);
                    }
                    break;
                case TargeterType.Potion:
                    //make a state that can pick up potions
                    this.Manager.Push(new PotionTargetState(this.UI, targeter as PotionTargeter, Targets));
                    break;
                case TargeterType.Tile:
                    this.Manager.Push(new TileTargetState(this.UI, targeter as TileTargeter, Targets));
                    break;
                case TargeterType.Box:
                    this.Manager.Push(new BoxTargetState(this.UI, targeter as BoxTargeter, Targets));
                    break;
                case TargeterType.Direction:
                    this.Manager.Push(new DirectionTargetState(this.UI, targeter as DirectionTargeter, Targets));
                    break;
                default:
                    //no known targeter type, guess we're done?
                    UI.StateManager.Exit(this);
                    //TODO slash XXX --- this can create some BUSTED STATE if we get here 
                    //in a group targeter, as early targeters will make states and then
                    //we get here and bail very ungracefully, leaving the old states alive
                    return;

            }


        }


        #region targeting substates
        abstract class TargetingSubState<TTargeter> : State where TTargeter : Targeter {

            protected TTargeter Targeter;
            protected IDictionary<TargeterBase, object> Targets;
            protected TargetingManager TargetingManager;
            protected string InstructionText;

            protected TargetingSubState(MatchUI ui, TTargeter targeter, IDictionary<TargeterBase, object> targets, string instructionText) : base(ui) {
                Targeter = targeter;
                Targets = targets;
                if (targeter.TargetDescription != "") {
                    this.InstructionText = "Select " + targeter.TargetDescription;
                } else {
                    this.InstructionText = instructionText;
                }
                this.TargetingManager = new TargetingManager();
            }

            public abstract override void Update();
            public override void Enter() {
                base.Enter();
                UI.UserPromptText.text = this.InstructionText;
            }

            public override void Resume() {
                base.Resume();
                UI.UserPromptText.text = this.InstructionText;
            }


            protected void HandleResponse(Message message) {
                var response = message as Command;
                if (response.Errors.Count == 0) {
                    // checks out! 
                    // we've already modified the target dict, let's just bounce from this state
                    this.Manager.Exit(this);
                    return;
                } else {
                    this.TargetingManager.RemoveTarget(this.Targeter, this.Targets);
                    this.Resume();
                }
            }

            protected void ValidateTargeter() {
                this.Pause();
                var command = new ValidateTargeterCommand(null,  this.Targeter, this.Targets);
                command.OnFinish += delegate { this.HandleResponse(command); };
                this.UI.MessageProcessor.FireMessage(command);
            }
        }

        class PotionTargetState : TargetingSubState<PotionTargeter> {
            Ray Ray;
            RaycastHit2D Hit;
            GameObject ClickedObject;

            public PotionTargetState(MatchUI ui, PotionTargeter targeter, IDictionary<TargeterBase, object> targets) : base(ui, targeter, targets, "Select a potion") {
            }



            public override void Update() {
                if (!this.IsActive) {
                    this.ClickedObject = null;
                    return;
                }

                Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Hit = Physics2D.Raycast(Ray.origin, Ray.direction);

                if (Hit != default(RaycastHit2D)) {  // we have a raycast hit
                    GameObject hitObject = Hit.collider.gameObject;
                    if (Input.GetMouseButtonDown(0)) {
                        ClickedObject = hitObject;
                    }

                    if (Input.GetMouseButtonUp(0)) {
                        if (hitObject == ClickedObject) {
                            //good click, check it out
                            if (hitObject.CompareTag("Potion")) {
                                this.TargetingManager.AddTarget(this.Targeter, this.Targeter.CreatePackage(hitObject.GetComponent<CellTransform>().Position), this.Targets);
                                this.ValidateTargeter();

                            }
                        }
                    }
                } else if (Input.GetMouseButtonDown(0)) {
                    ClickedObject = null;
                }
            }
        }

        class TileTargetState : TargetingSubState<TileTargeter> {
            Ray Ray;
            RaycastHit2D Hit;
            GameObject ClickedObject;
            int LayerMask;

            public TileTargetState(MatchUI ui, TileTargeter targeter, IDictionary<TargeterBase, object> targets) : base(ui, targeter, targets, "Select a tile") {
                this.LayerMask = UnityEngine.LayerMask.GetMask("Board");
            }


            public override void Update() {
                Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Hit = Physics2D.Raycast(Ray.origin, Ray.direction, Mathf.Infinity, this.LayerMask);

                if (Hit != default(RaycastHit2D)) {  // we have a raycast hit
                    GameObject hitObject = Hit.collider.gameObject;
                    if (Input.GetMouseButtonDown(0)) {
                        ClickedObject = hitObject;
                    }
                    if (Input.GetMouseButtonUp(0)) {
                        if (hitObject == ClickedObject) {
                            //good click, check it out
                            this.TargetingManager.AddTarget(this.Targeter, this.Targeter.CreatePackage(hitObject.GetComponent<CellTransform>().Position), this.Targets);
                            this.ValidateTargeter();
                        }
                    }
                } else if (Input.GetMouseButtonDown(0)) {
                    ClickedObject = null;
                }
            }
        }

        class DirectionTargetState : TargetingSubState<DirectionTargeter> {

            Dictionary<DirectionTargeter.DirectionChoice, UnityAction> ButtonCallbacks = new Dictionary<DirectionTargeter.DirectionChoice, UnityAction>(4); //so we can unregister them
            Dictionary<DirectionTargeter.DirectionChoice, Vector2Int> DirectionVectors = new Dictionary<DirectionTargeter.DirectionChoice, Vector2Int>() {
                {DirectionTargeter.DirectionChoice.North, Vector2Int.up },
                {DirectionTargeter.DirectionChoice.East, Vector2Int.right },
                {DirectionTargeter.DirectionChoice.South, Vector2Int.down },
                {DirectionTargeter.DirectionChoice.West, Vector2Int.left }

            };



            public DirectionTargetState(MatchUI ui, DirectionTargeter targeter, IDictionary<TargeterBase, object> targets) : base(ui, targeter, targets, "choose a direction") {
            }

            void ChooseDirection(DirectionTargeter.DirectionChoice direction) {
                this.TargetingManager.AddTarget(this.Targeter, this.Targeter.CreatePackage(this.DirectionVectors[direction]), this.Targets);
                this.ValidateTargeter();

            }

            public override void Enter() {
                base.Enter();
                //hook into the direction buttons and enable them
                foreach (DirectionTargeter.DirectionChoice direction in this.UI.DirectionButtons.Keys) {
                    Button button = this.UI.DirectionButtons[direction];
                    UnityAction action = new UnityAction(delegate { this.ChooseDirection(direction); });
                    button.onClick.AddListener(action);
                    this.ButtonCallbacks.Add(direction, action);
                    button.gameObject.SetActive(true);
                }
            }
            public override void Exit() {
                base.Exit();
                this.Pause();
                foreach (var kvp in this.ButtonCallbacks) {
                    this.UI.DirectionButtons[kvp.Key].onClick.RemoveListener(kvp.Value);
                }
            }

            public override void Pause() {
                base.Pause();
                //disable the buttons
                foreach (Button button in this.UI.DirectionButtons.Values)
                    button.gameObject.SetActive(false);
            }

            public override void Resume() {
                base.Resume();
                //enable the buttons
                foreach (Button button in this.UI.DirectionButtons.Values)
                    button.gameObject.SetActive(true);
            }

            public override void Update() {

            }
        }

        class BoxTargetState : TargetingSubState<BoxTargeter> {

            Ray Ray;
            RaycastHit2D Hit;
            GameObject ClickedObject;
            int LayerMask;

            public BoxTargetState(MatchUI ui, BoxTargeter targeter, IDictionary<TargeterBase, object> targets) : base(ui, targeter, targets, instructionText: "Select a Box") {
                this.LayerMask = UnityEngine.LayerMask.GetMask("UI");
            }

            public override void Enter() {
                foreach (SpriteRenderer r in this.UI.InterTileBoxes.Values) {
                    r.gameObject.SetActive(true);
                }
            }
            public override void Exit() {
                foreach (SpriteRenderer r in this.UI.InterTileBoxes.Values) {
                    r.gameObject.SetActive(false);
                }
            }

            public override void Update() {
                Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Hit = Physics2D.Raycast(Ray.origin, Ray.direction, Mathf.Infinity, this.LayerMask);

                if (Hit != default(RaycastHit2D)) {  // we have a raycast hit
                    GameObject hitObject = Hit.collider.gameObject;
                    //TODO -- put some visual cue here
                    //if (hitObject.CompareTag("InterTile") )
                    if (Input.GetMouseButtonDown(0)) {
                        ClickedObject = hitObject;
                    }
                    if (Input.GetMouseButtonUp(0)) {
                        if (hitObject == ClickedObject) { // have a full click
                            //get the position, use that to get the full set of targeted position
                            CellTransform transform = hitObject.GetComponent<CellTransform>();
                            if (transform == null)
                                return;


                            //got the positions, bounce
                            this.TargetingManager.AddTarget(this.Targeter, this.Targeter.CreatePackage(new BoxTargeter.BoxTargetInfo(transform.Position)), this.Targets);
                            this.ValidateTargeter();

                        }
                    }
                }
            }
        }
        #endregion
    }

    class PlacePotionStackState : State {

        ResolveMatchRequest Request;
        HashSet<Vector2Int> MatchPositions;
        GameObject ClickedObject;
        bool IsSelectDown { get { return Input.GetMouseButtonDown(0); } }
        bool IsSelectUp { get { return Input.GetMouseButtonUp(0); } }
        object MessageBlocker { get { return this; } }
        bool NeedsTarget = true;


        public PlacePotionStackState(MatchUI ui, ResolveMatchRequest request) : base(ui) {
            this.Request = request;
            this.MatchPositions = new HashSet<Vector2Int>(request.MatchMessage.Positions);
            request.AddBlocker(this.MessageBlocker);
            request.OnFinish += delegate { this.Manager.Exit(this); };
        }

        void HandleResponse(Message response) {
            if ( (response as Command ).Errors.Count == 0) {
                this.Request.RemoveBlocker(this.MessageBlocker);
                //this.Manager.Exit(this);
            } else {
                this.UI.UserPromptText.text = "Invalid spot for potion stack, try again";
                this.NeedsTarget = true;
            }
        }


        public override void Update() {
            if (!this.NeedsTarget)
                return;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, LayerMask.GetMask("Board"));
            if(hit != default(RaycastHit2D)) {
                GameObject hitObject = hit.collider.gameObject;
                if (this.IsSelectDown) {
                    this.ClickedObject = hitObject; 
                } else if (IsSelectUp) {
                    if (hitObject == this.ClickedObject) {
                        CellTransform cellTransform = hitObject.GetComponent<CellTransform>();
                        /*
                         * Right here we need to:
                         * - fire off the command
                         * - set the callback to either be cool and close state or just resume and try again
                         * 
                         */
                        this.NeedsTarget = false;
                        this.ClickedObject = null;
                        var command = new ResolveMatchCommand(Request, Request.MatchMessage, cellTransform.Position  );
                        command.OnFinish += delegate { this.HandleResponse(command); };
                        this.UI.MessageProcessor.EnqueueMessage(command);
                        return;

                    } else { //improper click
                        this.ClickedObject = null;
                    }
                }

            } else if (Input.GetMouseButtonDown(0)) {
                this.ClickedObject = null;
            }


        }

        public override void Enter() {
            base.Enter();
            this.UI.UserPromptText.text = "Pick Match Spot";
        }

    }

    class RestockState : State {
        Ray Ray;
        RaycastHit2D Hit;
        GameObject ClickedObject;
        int LayerMask;
        RestockRequest Request;
        object MessageBlocker { get { return this; } }

        public RestockState(MatchUI ui, RestockRequest request) : base(ui) {
            this.LayerMask = UnityEngine.LayerMask.GetMask("Board");
            this.Request = request;
            this.Request.AddBlocker(this.MessageBlocker);
        }

        public override void Enter() {
            base.Enter();
            this.UI.UserPromptText.text = "Select Restock Position";
        }

        public override void Resume() {
            base.Resume();
            this.UI.UserPromptText.text = "Select Restock Position";
        }


        //TODO -- instead of doing the "is active, choose text" thing, set an instance field
        // for the text to use? Maybe? Would still need to know whether or not to change it NOW
        void HandleResponse(Message response) {
            var command = response as Command;
            if (command.Errors.Count == 0) {
                this.Request.RemoveBlocker(this.MessageBlocker);
                this.Manager.Exit(this);
                return;
            } else {
                if (this.IsActive)
                    this.UI.UserPromptText.text = command.Errors[0].Message;
            }
        }


        public override void Update() {
            Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Hit = Physics2D.Raycast(Ray.origin, Ray.direction);
            if (Hit != default(RaycastHit2D)) {  // we have a raycast hit
                GameObject hitObject = Hit.collider.gameObject;
                if (Input.GetMouseButtonDown(0)) {
                    ClickedObject = Hit.collider.gameObject;
                }
                if (Input.GetMouseButtonUp(0)) {
                    if (hitObject == ClickedObject) { //  got a winner, do it and bounce
                        var position = hitObject.GetComponent<CellTransform>().Position;

                        // make the command and send it here
                        var command = new RestockCommand(this.Request, position);
                        command.OnFinish += delegate { this.HandleResponse(command); };
                        this.UI.MessageProcessor.EnqueueMessage(command);
                    }
                }
            } else if (Input.GetMouseButtonDown(0)) {
                ClickedObject = null;
            }

        }

    }

    class SatisfyApothecaryState : State {

        ICollection<KeyValuePair<Apothecary, UnityAction>> Callbacks = new NodeList<KeyValuePair<Apothecary, UnityAction>>();
        SatisfyApothecaryRequest Request;
        bool PendingCommand = false;
        object MessageBlocker { get { return this; } }

        public SatisfyApothecaryState(MatchUI ui, SatisfyApothecaryRequest request) : base(ui) {
            this.Request = request;
            this.Request.AddBlocker(this.MessageBlocker);
            request.OnFinish += delegate { this.Manager.Exit(this); };
        }

        void HandleResponse(Message response) {
            if( (response as Command).Errors.Count == 0) {
                this.Request.RemoveBlocker(this.MessageBlocker);
                //this.Manager.Exit(this);
            } else {
                this.PendingCommand = false;
                if (this.IsActive)
                    this.UI.UserPromptText.text = "RETRY: Satisfy an Apothecary";
            }
                
        }

        public override void Enter() {
            base.Enter();
            this.UI.UserPromptText.text = "Satisfy an Apothecary";
            //hook on to the apothecary buttons
            foreach (ApotehecaryEntry  entry in UI._apothecaryEntries.Values) {

                UnityAction callback = delegate { this.OnSelectApothecary(entry.Apothecary); };
                entry.Button.onClick.AddListener(callback);
                this.Callbacks.Add(new KeyValuePair<Apothecary, UnityAction>( entry.Apothecary, callback));
            }
        }

        public override void Update() {
            //throw new System.NotImplementedException();
        }


        public override void Exit() {
            base.Exit();
            foreach(KeyValuePair<Apothecary, UnityAction> kvp in this.Callbacks) {
                ApotehecaryEntry entry;
                if (this.UI._apothecaryEntries.TryGetValue(kvp.Key, out entry)) {
                    entry.Button.onClick.RemoveListener(kvp.Value);
                }
            }
        }

        void OnSelectApothecary(Apothecary apothecary) {
            if (!this.IsActive || this.PendingCommand)
                return;

            //send command for validation
            this.PendingCommand = true;
            var command = new SatisfyApothecaryCommand(null,  apothecary);
            command.OnFinish += delegate { this.HandleResponse(command); };
            this.UI.MessageProcessor.EnqueueMessage(command);
           
        }
    }

    class GameOverState : State {
        public GameOverState(MatchUI ui) : base(ui) {
        }


        public override void Enter() {
            base.Enter();
            this.UI.UserPromptText.text = "GAME OVER!";
        }

        public override void Update() {
            
        }
    }











    #endregion





}
