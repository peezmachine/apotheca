﻿using UnityEngine;
using VoidEventHandler = Utility.VoidEventHandler;



// The Notifier class is mostly for propagating things like Unity collision events so they can be handled by GameObjects other than
// the one with the collider on it.


public class Notifier : MonoBehaviour {


    public delegate void Collider2DEventHandler(Collider2D c);
    

    event Collider2DEventHandler _onTriggerEnter2D = delegate { };
    event Collider2DEventHandler _onTriggerStay2D = delegate { };
    event Collider2DEventHandler _onTriggerExit2D = delegate { };

    event VoidEventHandler _onMouseOver = delegate { };
    event VoidEventHandler _onMouseUpAsButton = delegate { };

    // naming on these has "Signal" at the end so as not to conflict with the existing Unity events.
    //physics events
    public event Collider2DEventHandler OnTriggerEnter2DSignal { add { _onTriggerEnter2D += value; } remove { _onTriggerEnter2D -= value; } }
    public event Collider2DEventHandler OnTriggerStay2DSignal { add { _onTriggerStay2D += value; } remove { _onTriggerStay2D -= value; } }
    public event Collider2DEventHandler OnTriggerExit2DSignal { add { _onTriggerExit2D += value; } remove { _onTriggerExit2D -= value; } }
    

    //mouse events
    public event VoidEventHandler OnMouseOverSignal { add { _onMouseOver += value; } remove { _onMouseOver -= value; } }
    public event VoidEventHandler OnMouseUpAsButtonSignal { add { _onMouseUpAsButton += value; } remove { _onMouseUpAsButton -= value; } }


    void OnTriggerEnter2D(Collider2D c) {
        _onTriggerEnter2D.Invoke(c);
    }

    void OnTriggerStay2D(Collider2D c) {
        _onTriggerStay2D.Invoke(c);
    }

    void OnTriggerExit2D(Collider2D c) {
        _onTriggerExit2D.Invoke(c);
    }

    void OnMouseOver() {
        _onMouseOver.Invoke();
    }

    void OnMouseUpAsButton() {
        _onMouseUpAsButton.Invoke();
    }


}
