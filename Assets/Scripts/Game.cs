﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Collections;
using EzPz.Messaging;
using Math = System.Math;
using Action = System.Action;


public class Game : MonoBehaviour {


    //For the love of all that is holy, positions are X,Y,     [[[NOT row, col]]]

    [SerializeField]
    SolitaireMatchModel _matchModel;

    [SerializeField] SceneInitializer _sceneInitializer; //provides access point to event system, other cross-scene objects

    GameObject _potionTilePrefab;

    public int Score;

    public MessageProcessor MessageProcessor { get; set; }

    public SubscriptionBroker SubscriptionBroker { get; set; }

    


    void Awake() {

        //make match
        if (_matchModel == null) {
            GameObject _matchModelEntity = new GameObject("match");
            _matchModel = _matchModelEntity.AddComponent<SolitaireMatchModel>();

            //make player
            GameObject _playerEntity = new GameObject("player");
            _matchModel.Player = _playerEntity.AddComponent<Player>();
            GameObject apothecaryEntity = new GameObject("apothecary");
            apothecaryEntity.AddComponent<Apothecary>();
        }
       
    }


    private void Start() {

        this.MessageProcessor = this._sceneInitializer.MessageManagerProvider.Processor;
        this.SubscriptionBroker = this._sceneInitializer.MessageManagerProvider.BrokerProvider.Broker;

        //event hook-in
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.HandlePotionsMovedMessage(m as PotionsMovedMessage)); }, typeof(PotionsMovedMessage));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.OnPotionsMatched(m as PotionsMatchedMessage)); }, typeof(PotionsMatchedMessage));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.HandlePotionRevealedMessage(m as PotionRevealedMessage)); }, typeof(PotionRevealedMessage));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { this.HandlePlayerActionTakenMessage(m as PlayerActionTakenMessage); }, typeof(PlayerActionTakenMessage));


        //command hook-in
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.ResolveMatch(m as ResolveMatchCommand)); }, typeof(ResolveMatchCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.HandleRestockCommand(m as RestockCommand)); }, typeof(RestockCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { this.HandlesSatisfyApothecaryCommand(m as SatisfyApothecaryCommand); }, typeof(SatisfyApothecaryCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { this.HandleValidateTargeterCommand(m as ValidateTargeterCommand); }, typeof(ValidateTargeterCommand));

        // player action commands
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.HandleRevealActionCommand(m as RevealActionCommand)); }, typeof(RevealActionCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { StartCoroutine(this.HandleApothecaryActionCommand(m as ApothecaryActionCommand)); }, typeof(ApothecaryActionCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { this.HandleRestockActionCommand(m as RestockActionCommand); }, typeof(RestockActionCommand));
        this.SubscriptionBroker.Subscribe(delegate (Message m) { this.HandleHireActionCommand(m as HireApothecaryActionCommand); }, typeof(HireApothecaryActionCommand));

    }




    void MovePotions(ICollection<KeyValuePair<Vector2Int, Vector2Int>> moves, Message parentMessage) {
        //claim a block on the parent message until the moves have all been addressed
        var blocker = new object();
        parentMessage.AddBlocker(blocker);

        var validMoves = new Dictionary<Potion, KeyValuePair<Vector2Int, Vector2Int>>(moves.Count);
        var firedMoves = new List<PotionsMovedMessage.PotionMovementInfo>(moves.Count);
        //TODO -- add smarter fixing for cases where one or more of the moves fails
        // --> might just need to track the moves we've already made, roll them back,
        // then remove the failed move from the list, and run the moves again.

        foreach (KeyValuePair<Vector2Int, Vector2Int> move in moves) {
            Potion potion;
            if (_matchModel.Potions.TryGetValue(move.Key, out potion)){
                _matchModel.Potions.Remove(move.Key);
                validMoves[potion] = move;
            }
        }

        foreach(KeyValuePair< Potion, KeyValuePair < Vector2Int, Vector2Int>> move in validMoves) {
            _matchModel.Potions[move.Value.Value] = move.Key;
            firedMoves.Add(new PotionsMovedMessage.PotionMovementInfo(move.Value.Key, move.Value.Value, move.Key));
        }

        var outgoingMessage = new PotionsMovedMessage(parentMessage, firedMoves.ToArray());
        outgoingMessage.OnFinish += delegate { parentMessage.RemoveBlocker(blocker); };
        this.MessageProcessor.EnqueueMessage(outgoingMessage);
    }


    private void RevealPotion(Vector2Int position, Message parent) {
        //block the parent until the Reveal message is done
        var blocker = new object();
        parent.AddBlocker(blocker);



        Potion potion = _matchModel.Potions[position];
        potion.IsRevealed = true;

        var message = new PotionRevealedMessage(parent,  position);
        message.OnFinish += delegate { parent.RemoveBlocker(blocker); };
        this.MessageProcessor.EnqueueMessage(message);
    }


    void DealPotion(Stack<Potion> deck, Vector2Int position, bool isRevelead = true) {
        Potion potion = deck.Pop();
        potion.IsRevealed = isRevelead;
        _matchModel.Potions[position] = potion; 
    }


    ICollection<HashSet<Vector2Int>> FindPotionMatches() {

        //horizontal check -- check the left columns, look to the right
        //vertical check -- check the bottow rows, look up

        //TODO -- use the array size instead of fixed size of 4x4
        
        var matches = new List<PotionMatch>();
        PotionMatch match;

        //horizontal check
        for (int x = 0; x < 2; x++) {

            for (int y = 0; y < 4; y++) {
                match = matchFinderHelper(new Vector2Int( x, y) , Vector2Int.right );
                if (match != null)
                    matches.Add(match);
            }


        }
        //vertical check
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y<2; y++) {
                match = matchFinderHelper(new Vector2Int(x, y), Vector2Int.up);
                if (match != null)
                    matches.Add(match);
            }
        }


        /*
         * SUPREME NOTE:
         * We don't need to look for special matches specifically!
         * Any two matches that overlap form a special match
         * 
         * 
         * 
         */

        //now we need to compare each match against all the matches after it
        //if they overlap, create a super match from their union
        List<HashSet<Vector2Int>> finalMatches = new List<HashSet<Vector2Int>>();
        HashSet<HashSet<Vector2Int>> countedMatches = new HashSet<HashSet<Vector2Int>>();
        //countedMatches has the 3-matches that have already been rolled into a special match
        for (int i = 0; i<matches.Count; i++) {
            var setA = matches[i].PotionPositions;
            if (countedMatches.Contains(setA))
                continue;

            for (int j = i+1; j < matches.Count; j++) {
               
                var setB = matches[j].PotionPositions;

                if (setA.Overlaps(setB)) {
                    setA.UnionWith(setB);
                    countedMatches.Add(setB); //won't include it in other matches
                    break;
                    //XXX -- not allowing a set to match with 2 downstream sets.
                    //probably fine for all but the nuttiest edge cases, but worth a look
                }
            }

            finalMatches.Add(setA);

        }

 

        return finalMatches;
    }


    PotionMatch matchFinderHelper(Vector2Int startPosition, Vector2Int direction) {
        int foundCount = 0;

        int currentX = startPosition.x;
        int currentY = startPosition.y;
        Potion currentPotion;
        Potion.PotionColor matchColor = default(Potion.PotionColor);

        //var positions = new List<Vector2Int>();

        bool currentCheckFinished = false;
        while (!currentCheckFinished) {
            Vector2Int currentPosition = new Vector2Int(currentX, currentY);
            bool havePotion = _matchModel.Potions.TryGetValue(currentPosition, out currentPotion);
            if (havePotion && currentPotion.IsRevealed) {
                if (foundCount == 0) {
                    foundCount++;
                    matchColor = currentPotion.Color;
                    //positions.Add(currentPosition);
                } else {
                    if (currentPotion.Color == matchColor) {
                        foundCount++;
                        //positions.Add(currentPosition);
                    } else { //got the wrong color!
                        currentCheckFinished = true;
                    }
                }
            } else { // no (or no revealed) potion here!
                currentCheckFinished = true;
            }
                //let's move in the indicated direction, even if the loop exits here
            currentX += direction.x;
            currentY += direction.y;


            if (foundCount >= 3)
                return new PotionMatch(startPosition, direction); 
        } // end loop

        //got here without a match, too bad!
        return null;


    }




    public static int TileDistance(Vector2Int a, Vector2Int b, bool countDiagonalAsOne) {


        int xDiff = Math.Abs(a.x - b.x);
        int yDiff = Math.Abs(a.y - b.y);


        if (!countDiagonalAsOne) {
            return xDiff + yDiff;
        } else {
            //break the distance up into a diagonal component and a straight component for the remainder
            int diagCount = Math.Min(xDiff, yDiff);
            int straightCount = Math.Max(xDiff, yDiff) - diagCount;
            return diagCount + straightCount;
        }
    }


    class PotionMatch {

        public Vector2Int StartPosition;
        public Vector2Int Direction;
        //can then generate (and cache) list of included positions from this

        HashSet<Vector2Int> _positions; //cached

        public HashSet<Vector2Int> PotionPositions {
            get {
                if (_positions == null) {
                    _positions = new HashSet<Vector2Int>();
                    for(int i = 0; i < 3; i++) {
                        Vector2Int p = StartPosition + (Direction * i);
                        _positions.Add(p);
                    }
                }
                return _positions;
            }
        }


        public PotionMatch(Vector2Int startPosition, Vector2Int direction, params Vector2Int[] specialPositions) {
            StartPosition = startPosition;
            Direction = direction;
            //SpecialPositions = new List<Vector2Int>(specialPositions);
        }
       

    }





    IEnumerator HandlePotionsMovedMessage(PotionsMovedMessage message) {
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(message, wait, blocker));
        yield return wait;


        //check for matches
        ICollection< HashSet<Vector2Int>>  matches = this.FindPotionMatches();
        if (matches.Count > 0) {
            foreach (HashSet < Vector2Int > match in matches) {
                wait.SetStatus(true); // keep waiting

                //bind the yield to message finish

                var outgoingMessage = new PotionsMatchedMessage(message, match);
                outgoingMessage.OnFinish += delegate { wait.SetStatus(false); };
                this.MessageProcessor.EnqueueMessage(outgoingMessage);

                while (wait.keepWaiting)
                    yield return wait;


            }
        }
        message.RemoveBlocker(blocker);
    }


    IEnumerator HandlePotionRevealedMessage(PotionRevealedMessage message) {
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(message, wait, blocker));
        yield return wait;

        //we are now processing the incoming message



        Potion potion = _matchModel.Potions[message.Position];
        _matchModel.Player.Gems[potion.Color]++;

        /*
         * XXX -- the "check for matches" code appears in two different places.
         * TODO -- factor it out -- might need the helper method we make
         * to take some extra args specifying the message-blocking behavior.
         * 
         */ 

        //check for matches
        ICollection<HashSet<Vector2Int>> matches = this.FindPotionMatches();
        if (matches.Count > 0) {
            foreach (HashSet<Vector2Int> match in matches) {
                wait.SetStatus(true); // keep waiting

                //bind the yield to message finish
                //block the incoming message until the outgoing one is resolved
                var newBlocker = new object();
                message.AddBlocker(newBlocker);
                var outgoingMessage = new PotionsMatchedMessage(message, match);
                outgoingMessage.OnFinish += delegate { wait.SetStatus(false); message.RemoveBlocker(newBlocker); };
                this.MessageProcessor.EnqueueMessage(outgoingMessage);

                while (wait.keepWaiting)
                    yield return wait;


            }
        }
        message.RemoveBlocker(blocker);
    }



    IEnumerator OnPotionsMatched(PotionsMatchedMessage message) {
        //this message is blocked until the match has been resolved
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(message, wait, blocker));
        yield return wait;

        //we are now handling the incoming message

        //request a resolution, hold block on incoming message until request is done 
        wait.SetStatus(true);
        var request = new ResolveMatchRequest(message,  message);
        request.OnFinish += delegate { message.RemoveBlocker(blocker); };

        this.MessageProcessor.EnqueueMessage(request);


        //TODO -- remove potions (no resolve request) if it's an MP match
        yield break;
    }

    //TODO -- some of this can be moved out of the Resolve function and into the just Match function
    //After all, MP doesn't have a resolve, and all we need Resolve for is deciding where the 
    // lingering potion will be.
    IEnumerator ResolveMatch(ResolveMatchCommand command) {
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(command, wait, blocker));
        yield return wait;

        //we are now handling the message

        //validation: confirm the potion to keep is part of the match
        if (!command.MatchMessage.Positions.Contains(command.PositionToKeep)) {
            //bad command, add an error
            command.Errors.Add(new ValidationError());
            command.RemoveBlocker(blocker);
            yield break;
        }

        var infos = new List<PotionsRemovedMessage.Info>();
        Potion.PotionColor color =  _matchModel.Potions[command.PositionToKeep].Color;

        //special match check
        //TODO -- probably just move this to the PotionsMatched handler, as it 
        // doesn't REALLY have to do with the resolution
        if (command.MatchMessage.Positions.Count > 3) {
            _matchModel.Player.Gems[color]++;
        }

        //deal with the potions
        foreach (Vector2Int position in command.MatchMessage.Positions) {
            if (position != command.PositionToKeep) {
                //remove the potions
                Potion potion = _matchModel.Potions[position];
                //color = potion.Color;
                _matchModel.Potions.Remove(position);
                infos.Add(new PotionsRemovedMessage.Info(position, potion));
            }
        }

        
        var m = new PotionsRemovedMessage(command, null, infos.ToArray());
        wait.SetStatus(true);
        m.OnFinish += delegate { wait.SetStatus(false); };
        this.MessageProcessor.EnqueueMessage(m);
        //holding until the potion removal has been finished
        while (wait.keepWaiting) {
            yield return wait;
        }

       



        //if apo, score and satisfy. if not, get a gem instead
        if (_matchModel.Player.Apothecaries.Count > 0) {
            if (infos.Count > 0) {
                foreach (Potion potion in _matchModel.Potions.Values) {
                    if (potion.Color == color)
                        this.Score++;
                }
            }
            Debug.Log("Score is now " + this.Score);
            var request = new SatisfyApothecaryRequest(command);

            
            wait.SetStatus(true);
            request.OnFinish += delegate { wait.SetStatus(false); };
            this.MessageProcessor.EnqueueMessage(request);
            //holding until the satisfy request is finished
            while (wait.keepWaiting)
                yield return wait;

        } else { //no apo :(
            _matchModel.Player.Gems[color]++;
        }

        command.RemoveBlocker(blocker);
        //TODO -- should this throw some sort of "MatchResolved" message instead of the SatisfyRequest?
        // Probably.
    }


    static bool IsPositionOnBoard(Vector2Int pos) {
        return (pos.x < 4 && pos.x >= 0 && pos.y >= 0 && pos.y < 4);
    }

    //gives the position that corresponds to an off-the-board position that 
    Vector2Int GetWrapPosition(Vector2Int position, Vector2Int direction) {
        /*
         * Ok this is pretty wild, so settle in.
         * For horizontal and vertical wraps, we just have to get the appropriate modulo
         * and make sure we deal with a negative remainder if we have one.
         * 
         * The diagonal wrap is a bit trickier. What we do here is figure out the length of 
         * the "sash" across the board at that angle. For a given off-board position and direction,
         * we first create an equation for the corresponding line such that y = ax +b, where a should
         * be either 1 or -1. Then, for a 0-indexed board, we find S by the piecewise function
         * 
         * S = 4 - |b| ; a = 1
         * S = 4 - |b-3| ; a = -1
         * 
         * or S = 4 - |b - 1.5 * (a-1)|   for either a, but has a non-int in there.
         * 
         * s tells us how big a step we take back towards the board until we are back on it
         * (by reversing direction and using s as magnitude)
         */
        //TODO -- making a lot of assumptions here, wouldn't hurt to do some reality-checking
        int s = 4; //default for hor/ver
        if (Math.Abs(direction.x) == Math.Abs(direction.y)) {
            int a;
            //diagonal
            if (direction.x == direction.y) {
                a = 1;
            } else {
                a = -1;
            }
            int b = position.y - (a * position.x);

            if (a == 1) {
                s = 4 - Math.Abs(b);
            } else if (a == -1) {
                s = 4 - Math.Abs(b - 3);
            }
            Debug.LogFormat("for diag wrap, a = {0} b= {1} s = {2} ", a, b, s);


            //now we step back onto the board
            //TODO -- algebra this out instead of while looping
            //(not that we should really expect huge outliers)

            while (!IsPositionOnBoard(position)) {
                position -= direction * s;
            }
            return position;
        }

        //horizontal/vertical
        int x = position.x % s;
        int y = position.y % s;
        if (x < 0)
            x += s;
        if (y < 0)
            y += s;
        Debug.LogFormat("wrap function converted {0},{1} to {2},{3}", position.x, position.y, x, y);
        return new Vector2Int(x, y);

    }



    #region effects

    //TODO -- Redo all the Effect stuff so it doesn't take targeters. For example,
    // the move effect should take a list of kvp<v2int,v2int> that are the from/to coords.
    // we can always overload if there are different ways to apply affects based on target types,
    // but all of that should be sorted by the TARGETER PROCESSOR, not the effect.
    // ^^^ maybe don't do this. Pros and cons all around, give it a thought sesh.


    void ApplyEffect(Effect effect, IDictionary<TargeterBase, object> targets, Message parent) {
        switch (effect.Type) {
            case Effect.EffectType.Move:
                this.ApplyMoveEffect(effect as MoveEffect, targets, parent);
                break;
            case Effect.EffectType.MoveLine:
                this.ApplyMoveLineEffect(effect as MoveLineEffect, targets, parent);
                break;
            case Effect.EffectType.Rotate:
                this.ApplyRotateEffect(effect as RotateEffect, targets, parent);
                break;
            case Effect.EffectType.Tempest:
                this.ApplyTempestEffect(effect as TempestEffect, targets, parent);
                break;
            default: 
                Debug.Log("ApplyEffect did not recognize the effect type");
                return;
        }
    }

    void ApplyMoveEffect(MoveEffect effect, IDictionary<TargeterBase, object> targets, Message parent) {
        // move from Source to Destination
        Dictionary<Vector2Int, Vector2Int> moves = new Dictionary<Vector2Int, Vector2Int>();


        foreach (MoveEffect.MoveInfo info in effect.Moves) {
            //assuming the source is a potion and dest is a tile for now
            var source = (targets[info.Source] as TargetPackage<Vector2Int>).Target;
            var destination = (targets[info.Destination] as TargetPackage<Vector2Int>).Target;


            moves[source] = destination;
        }

        //var command = new MovePotionsCommand(moveInfos.ToArray());
        //this.MovePotions(command);
        this.MovePotions(moves, parent);

    }

    void ApplyMoveLineEffect(MoveLineEffect effect, IDictionary<TargeterBase, object> targets, Message parent) {


        var moves = new Dictionary<Vector2Int, Vector2Int>(4);

        //assuming position-and-direction targets
        var source = (targets[effect.Origin] as TargetPackage<Vector2Int>).Target;
        var directionStep = (targets[effect.Direction] as TargetPackage<Vector2Int>).Target;



        //XXX -- this is all assuming that these two points are ortho or diag. hopefully the targeter did its job!
        //now we need to get all the affected tiles

        /*
         * Improved System:
         * get the destination position. if it is out of bounds, then do the correction
         * (translate for ortho, reflect for diag) to the origin and try the displacement from THERE
         * 
         */ 
         for (int i = 0; i < 4; i++) {
            var fromPosition = source + (directionStep * i);
            if (moves.ContainsKey(fromPosition))
                break;
            if (!IsPositionOnBoard(fromPosition))
                fromPosition = this.GetWrapPosition(fromPosition, directionStep);
            var toPosition = fromPosition + (directionStep * effect.Amount);
            if (!IsPositionOnBoard(toPosition))
                toPosition = this.GetWrapPosition(toPosition, directionStep);
            moves[fromPosition] = toPosition;
        }

        this.MovePotions(moves, parent);

    }

    void ApplyTempestEffect(TempestEffect effect, IDictionary<TargeterBase, object> targets, Message parent) {
        // determine the direction, determine if we are moving rows or cols, then work back from there

        var pushDirection = (targets[effect.Direction] as TargetPackage<Vector2Int>).Target;

        if (!(pushDirection.x == 0 ^ pushDirection.y == 0))
            return; // only want movement in ortho line

         ; // the step take to get the next tile. the inverse of the direction
        Vector2Int lineStep; //the step taken between line, as in "done with row 2, now to row 3
        Vector2Int startPosition; //the first in the loop
        //normalizing the direction
        if (pushDirection.x != 0) { //horizontal effect
            pushDirection.Set(pushDirection.x / Math.Abs(pushDirection.x), pushDirection.y);
            lineStep = new Vector2Int(0, 1); // doing row-by-row 
            if (pushDirection. x > 0) { //pushing right
                startPosition = new Vector2Int(3, 0); //TODO -- accomodate variable board size?
            } else { //pushing left
                startPosition = new Vector2Int(0, 0);
            }
        } else  { //vertical effect
            lineStep = new Vector2Int(1, 0); // doing col-by-col 
            pushDirection.Set(pushDirection.x, pushDirection.y / Math.Abs(pushDirection.y));
            if (pushDirection.y > 0) { //pushing up
                startPosition = new Vector2Int(0, 3); //TODO -- accomodate variable board size?
            } else { //pushing down
                startPosition = new Vector2Int(0, 0);
            }
        }



        Vector2Int tileStep = pushDirection * -1;
        Dictionary<Vector2Int, Vector2Int> moves = new Dictionary<Vector2Int, Vector2Int>(16);
        HashSet<Vector2Int> occupiedPositions = new HashSet<Vector2Int>(); //for quickly finding the tiles that WILL be occupied
        for (int line = 0; line < 4; line++) {
            for (int i = 0; i < 4; i++) {
                bool keepGoing = true;
                Vector2Int fromPosition = startPosition + (tileStep * i) + (lineStep * line);
                Vector2Int currentFromPosition = fromPosition;
                if (!this._matchModel.Potions.ContainsKey(fromPosition))
                    continue; // no potion there, move along
                while (keepGoing) {
                    // if we get a potion at from position, try to push it by 1 in pushdirection
                    // if that position is on the board and not in occupied positions, then move it there
                    // and redo the loop with the toPosition as the new fromPosition
                    Vector2Int toPosition = currentFromPosition + pushDirection;
                    if (occupiedPositions.Contains(toPosition) || !IsPositionOnBoard(toPosition)) {
                        //either off the board or occupied, bounce
                        keepGoing = false;
                    }  else {
                        currentFromPosition = toPosition;
                    }
                }
                moves[fromPosition] = currentFromPosition;
                occupiedPositions.Add(currentFromPosition);
            }
        }
        this.MovePotions(moves, parent);


    }

    //TODO -- make this take a bunch of positions. right now it's just doubling as a box targeter.
    void ApplyRotateEffect(RotateEffect effect, IDictionary<TargeterBase, object> targets, Message parent) {
        Dictionary<Vector2Int, Vector2Int> moves = new Dictionary<Vector2Int, Vector2Int>(4);
        //assuming clockwise for now

        var start = (targets[effect.Source] as TargetPackage<BoxTargeter.BoxTargetInfo>).Target;


        //move the lowest-x, lowest y target up by (0,1). then rotate the move direction by 90 degrees
        //alternatiely, ASSUMING a 2x2 box, the center has the same coords as lower left, so no need to sort.
        Vector2Int moveDirection = Vector2Int.up;
        Vector2Int position = start.Center;
        for (int i= 0; i < 4; i++) {
            Vector2Int newPosition = position + moveDirection;
            moves[position] = newPosition;

            position = newPosition;
            moveDirection = new Vector2Int(moveDirection.y, -moveDirection.x); //90 rotation clockwise
        }

        this.MovePotions(moves, parent);
    }
    #endregion





    #region requisites

    bool ValidateRequisite(Requisite requisite, IDictionary<TargeterBase, object> targets) {
        

        switch (requisite.Type) {
            case Requisite.RequisiteType.Distance:
                return this.ValidateDistanceRequisite(requisite as DistanceRequisite, targets);
            case Requisite.RequisiteType.Position:
                return this.ValidatePositionRequisite(requisite as PositionRequisite, targets);
            case Requisite.RequisiteType.Distinctness:
                return this.ValidateDistinctnessRequisite(requisite as DistinctnessRequisite, targets);
            default:
                Debug.Log("ValidateRequisite had unrecognized requisite type");
                return false;
        }

    }

    bool ValidateDistanceRequisite(DistanceRequisite requisite, IDictionary<TargeterBase, object> targets) {
        var origin = targets[requisite.Origin] as TargetPackage<Vector2Int>;
        var destination = targets[requisite.Destination] as TargetPackage<Vector2Int>;

        Vector2Int a = origin.Target;
        Vector2Int b = destination.Target;

        bool allowDiag = (requisite.AllowDiagonal == DistanceRequisite.DiagonalPermissionType.Yes || requisite.AllowDiagonal == DistanceRequisite.DiagonalPermissionType.Only);

        int d = TileDistance(a, b, allowDiag);
        if (d >= requisite.MinDistance && d <= requisite.MaxDistance) {
            switch (requisite.Orientation) {
                case DistanceRequisite.OrientationType.Any:
                    break; //all good!
                case DistanceRequisite.OrientationType.L:
                    if (Math.Abs(Math.Abs(a.x - b.x) - Math.Abs(a.y - b.y)) == 1)
                        break;
                    return false;
                case DistanceRequisite.OrientationType.Line:
                    //either same x, same y, or same delta mags (diag)
                    bool ortho = ((a.x - b.x == 0) || ((a.y - b.y == 0)));
                    bool isDiag = (Math.Abs(a.x - b.x) == Math.Abs(a.y - b.y));
                    if (requisite.AllowDiagonal == DistanceRequisite.DiagonalPermissionType.Only && !isDiag)
                        return false;
                    if (ortho || (allowDiag && isDiag))
                        break;
                    return false;
                default:
                    return false;
            }
        } else { // bad distance, you lose!
            return false;
        }



        return true;



    }


    bool ValidatePositionRequisite(PositionRequisite requisite, IDictionary<TargeterBase, object> targets) {
        var target = targets[requisite.Targeter] as TargetPackage<Vector2Int>;
        Vector2Int position = target.Target;


        if (requisite.EdgeAllowance != PositionRequisite.EdgeAllowanceType.Either) {
            bool xOnEdge = (position.x == 0 || position.x == 3);
            bool yOnEdge = (position.y == 0 || position.y == 3);

            bool isOnEdge = (xOnEdge || yOnEdge);

            if (requisite.EdgeAllowance == PositionRequisite.EdgeAllowanceType.Yes && !isOnEdge)
                return false;

            if (requisite.EdgeAllowance == PositionRequisite.EdgeAllowanceType.No && isOnEdge)
                return false;

        }

        return true;
    }


    bool ValidateDistinctnessRequisite(DistinctnessRequisite requisite, IDictionary<TargeterBase, object> targets) {

        List<object> entries = new List<object>();
        foreach (var targeter in requisite.Entries) {
            object t = (targets[targeter] as ITargetPackage).Target;

            if (entries.Contains(t))
                return false;
            entries.Add(t);
        }
        return true;


    }

    #endregion

    #region targeters



    bool ValidateTarget(Targeter targeter, IDictionary<TargeterBase, object> targets) {
        bool isValid = false;
        switch (targeter.Type) {
            case TargeterType.TargeterGroup:
                isValid = this.ValidateGroupTarget(targeter as TargeterGroup, targets);
                break;
            case TargeterType.Potion:
                isValid = this.ValidatePotionTarget(targeter as PotionTargeter, targets);
                break;
            case TargeterType.Tile:
                isValid = this.ValidateTileTarget(targeter as TileTargeter, targets);
                break;
            case TargeterType.Direction:
                isValid = this.ValidateDirectionTarget(targeter as DirectionTargeter, targets);
                break;
            case TargeterType.Box:
                isValid = this.ValidateBoxTarget(targeter as BoxTargeter, targets);
                break;
            default:
                Debug.Log("ValidateTarget had unrecognized targeter type");
                return false; //couldn't find the targeter type!
        }

        if (!isValid)
            return false;

        //now validate the targeter's requisites
        foreach (Requisite requisite in targeter.Requisites) {
            if (!this.ValidateRequisite(requisite, targets))
                return false;

        }

        return isValid;

    }

    bool ValidateGroupTarget(TargeterGroup targeterGroup, IDictionary<TargeterBase, object> targets) {
        /*
         * for each of the group's targeters, see if the provided target works
         * 
         * 
         * 
         */


        foreach(Targeter targeter in targeterGroup.Targeters) {
            if (!this.ValidateTarget(targeter, targets))
                return false;
        }

        return true;

    }

    //TODO -- null check the hell out of this thing
    // targets SHOULD all be ITargetHolder<TTarget>, so cast to that as needed
    //XXX TODO EPEPEP -- some BUG where this is getting hit after a potion has been removed
    // as part of a match resulting from apothecary ability.
     bool ValidatePotionTarget(PotionTargeter targeter, IDictionary<TargeterBase, object> targets) {
        Vector2Int position = (targets[targeter] as ITargetPackage<Vector2Int>).Target;
        Potion target = _matchModel.Potions[position];

        //check revealed
        switch (targeter.Revealed) {
            case PotionTargeter.RevealedStatus.Either:
                break;
            case PotionTargeter.RevealedStatus.No:
                if (target.IsRevealed)
                    return false;
                break;
            case PotionTargeter.RevealedStatus.Yes:
                if (!target.IsRevealed)
                    return false;
                break;
            default:
                return false;
        }


        return true;

    }

    public bool ValidateDirectionTarget(DirectionTargeter targeter, IDictionary<TargeterBase, object> targets) {
        return true;
    }

    public bool ValidateTileTarget(TileTargeter targeter, IDictionary<TargeterBase, object> targets) {
        Vector2Int position = (targets[targeter] as ITargetPackage<Vector2Int>).Target;
        if (targeter.MustBeEmpty && _matchModel.Potions.ContainsKey(position))
            return false;


        return true;
    }

    //TODO -- actually try a little on this one, buddy.
    public bool ValidateBoxTarget(BoxTargeter targeter, IDictionary<TargeterBase, object> targets) {
        return true;
    }

    #endregion



    #region command handlers
    IEnumerator HandleApothecaryActionCommand(ApothecaryActionCommand command) {
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(command, wait, blocker));
        yield return wait;

        /*
         * Determine the targeter type
         * cast the target to an appropriate type,
         * try to validate the target for that targeter
         * 
         * Note -- more complicated targeting systems might require some more context
         * about things like the player involved, etc. For later.
         * 
         */
        Targeter targeter = command.Apothecary.Ability.Targeter;

        //TODO -------- implement this
        bool isValid = false;
        switch (command.Apothecary.Ability.Targeter.Type) {
            case TargeterType.TargeterGroup:
                isValid = this.ValidateGroupTarget(targeter as TargeterGroup, command.Targets);
                break;
            case TargeterType.Box:
                isValid = this.ValidateBoxTarget(targeter as BoxTargeter, command.Targets);
                break;
            case TargeterType.Direction:
                isValid = this.ValidateDirectionTarget(targeter as DirectionTargeter, command.Targets);
                break;
            default:
                Debug.Log("Apothecary command had unrecognized targeter type");
                break;

        }


        //Can avoid having to cast the targeter in the ApplyEffect call if we move this chunk
        // up to the other part. would have to define a bunch of ApplyEffect(specificTargeter...) methods
        //NOTE -- not really! our targeter-effect system is many-to-many. 
        // NOTE TO SELF -- you will probably forget this, but you spent a while considering ways to rework
        // the targeting system and decided that while this system requires casting the targeters twice
        // (once to validate, then again when processing an effect), it provides a SUPER TON of 
        // flexibility. Keep it!!!!
        if (!isValid) {
            command.RemoveBlocker(blocker);
            yield break;
        }
            
        foreach (Effect effect in command.Apothecary.Ability.Targeter.Effects) {
            this.ApplyEffect(effect, command.Targets, command);
        }


        //when everyone is done with the command, fire the message

        var postMessage = new PlayerActionTakenMessage(command);
        command.OnFinish += delegate { this.MessageProcessor.EnqueueMessage(postMessage); };

        command.RemoveBlocker(blocker);

    }

    IEnumerator HandleRevealActionCommand(RevealActionCommand command) {
        var wait = new YieldInstruction(true);
        var blocker = new object();

        StartCoroutine(WaitForUnblock(command, wait, blocker));
        yield return wait;

        //now we need to actually do the reveal, and hold until it is finished

        //when everyone is done with the command, fire the message
        var postMessage = new PlayerActionTakenMessage(command);
        command.OnFinish += delegate { this.MessageProcessor.EnqueueMessage(postMessage); };

        this.RevealPotion(command.Position, command); //this will add a block to the command, so we can move on

        command.RemoveBlocker(blocker);
        yield break;
    }

    void HandleRestockActionCommand(RestockActionCommand command) {
        var request = new RestockRequest(command, 1);
        this.MessageProcessor.EnqueueMessage(request);


    }

    IEnumerator HandleRestockCommand(RestockCommand command) {
        List<Vector2Int> addedPositions = new List<Vector2Int>(command.Restocks.Length);

        //validation goes here
        foreach (Vector2Int restock in command.Restocks) {
            //make sure the target is empty
            if (_matchModel.Potions.ContainsKey(restock)) {
                command.Errors.Add(new ValidationError("Restock target must be empty"));
                yield break; 
            }


            //if single-player, make sure the supply is available
            if (!_matchModel.Supply.ContainsKey(restock)) {
                command.Errors.Add(new ValidationError("Supply there is empty!"));
                yield break;
            }
        }

        if (command.ValidateOnly)
            yield break;

        foreach (Vector2Int restock in command.Restocks) {
            addedPositions.Add(restock);
            Potion potion = _matchModel.Supply[restock];
            potion.Owner = _matchModel.Player;
            _matchModel.Supply.Remove(restock);
            _matchModel.Potions[restock] = potion;
        }

        this.MessageProcessor.EnqueueMessage(new PotionsAddedMessage(command, null, addedPositions.ToArray()));

    }

    void HandlesSatisfyApothecaryCommand(SatisfyApothecaryCommand command) {
        //put validation here
        if (command.ValidateOnly)
            return;


        _matchModel.Player.Apothecaries.Remove(command.Apothecary);
    }

    void HandleHireActionCommand(HireApothecaryActionCommand command) {
        int cost = 3;
        int have = _matchModel.Player.Gems[command.Color];
        //for solitaire, only allow single-color buys from the alley
        if (have < cost)
            return;

        _matchModel.Player.Gems[command.Color] = have - 3;
        _matchModel.Player.Apothecaries.Add(_matchModel.ApothecariesForSale[command.Color]);
        _matchModel.ApothecariesForSale[command.Color] = _matchModel.ApothecaryDeck.Pop();

        //when everyone is done with the command, fire the message
        var postMessage = new PlayerActionTakenMessage(command);
        command.OnFinish += delegate { this.MessageProcessor.EnqueueMessage(postMessage); };
    }

    void HandleValidateTargeterCommand(ValidateTargeterCommand command) {
        if (!this.ValidateTarget(command.Targeter, command.Targets)) {
            command.Errors.Add(new ValidationError("targeter validation error"));
        }

        return;

        /*
         * Need the validation-checkers to return validation error(s)
         * so we can attach them to the command here.
         */
    }



    #endregion


    void HandlePlayerActionTakenMessage(PlayerActionTakenMessage message) {
        _matchModel.MovesLeft--;

        if (_matchModel.MovesLeft <= 0)
            this.EndTurn();
        Debug.Log("MOVES LEFT: " + _matchModel.MovesLeft);

    }



    void EndTurn() {
        //if no restock is available, game is over
        if (_matchModel.Supply.Count == 0) {
            this.EndGame();
            return;
        }

        //see if there's an empty space free for any of the restock potions
        bool restockAvailable = false;
        foreach(Vector2Int position in _matchModel.Supply.Keys) {
            if (!this._matchModel.Potions.ContainsKey(position)) {
                restockAvailable = true;
                break;
            }
        }
        if (!restockAvailable) {
            this.EndGame();
            return;
        }

        

        _matchModel.MovesLeft = 2;
        
        this.MessageProcessor.EnqueueMessage(new RestockRequest(null, 1));
        
    }

    void EndGame() {
        Debug.Log("GAME OVER MAN your score is: " + this.Score);
        var message = new GameOverMessage(null);
        this.MessageProcessor.EnqueueMessage(message);
    }

    //Solitaire-specific restock behavior here
    void RestockPotions(ICollection<KeyValuePair<Vector2Int, Potion>> restocks, Message parentMessage = null) {

    }


    /* WaitForUnblock
     * Helper method since a lot of event handlers need to wait for an unblocked message
     */ 
    IEnumerator WaitForUnblock(Message message, YieldInstruction wait, object blocker) {

        if (message.IsBlocked) {
            wait.SetStatus(true);
            System.Action a = delegate {
                if (!message.IsBlocked) {
                    message.AddBlocker(blocker);
                    wait.SetStatus(false);
                };
            };

            message.OnUnblock += a;
            while (wait.keepWaiting)
                yield return wait;
            message.OnUnblock -= a;
        } else { //message isn't blocked, let's claim it and bounce
            message.AddBlocker(blocker);
            wait.SetStatus(false);
        }
    }



}



public class YieldInstruction : CustomYieldInstruction {
    bool _keepWaiting;

    public override bool keepWaiting { get { return _keepWaiting; } }


    public YieldInstruction(bool keepWaiting = true) {
        _keepWaiting = keepWaiting;
    }

    public void SetStatus(bool keepWaiting) {
        _keepWaiting = keepWaiting;
    }
    
}
