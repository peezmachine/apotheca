﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Potion : MonoBehaviour {

    public enum PotionColor { Red, Blue, Yellow }


    public PotionColor Color;
    public bool IsRevealed = true;
    public Player Owner;
    
}

