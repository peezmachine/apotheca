﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apothecary : MonoBehaviour {

    [SerializeField] ApothecaryData _data;
   

    public string Name { get { return Data.Name; } }

    public Ability Ability { get { return Data.Ability; } }

    public ApothecaryData Data {
        private get {
            return _data;
        }

        set {
            _data = value;
        }
    }
}

