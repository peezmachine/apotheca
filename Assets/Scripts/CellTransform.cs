﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class CellTransform : MonoBehaviour {

    [SerializeField]
    Vector2Int _position;

    public Vector2Int Position {
        get {
            return _position;
        }

        set {
            _position = value;
        }
    }
}
