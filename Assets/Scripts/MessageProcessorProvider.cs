﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Messaging;
using System;

//Note -- using Message instead of IMessage throughout so we know we have ref-types for casting

public class MessageProcessorProvider : MonoBehaviour {

    Queue<Message> _messageQueue;





    [SerializeField]
    SubscriptionBrokerProvider _brokerProvider;

    MessageProcessor _messageProcessor;

    public MessageProcessor Processor { get; private set; }

    public Queue<Message> MessageQueue {
        get {
            return _messageQueue;
        }

        private set {
            _messageQueue = value;
        }
    }

    public SubscriptionBrokerProvider BrokerProvider {
        get {
            return _brokerProvider;
        }

        private set {
            _brokerProvider = value;
        }
    }


    //Construct should only be called when createing a message manager at runtime
    public void Construct(MessageProcessor processor, SubscriptionBroker broker, Queue<Message> queue) {
        this.Processor = processor;
        this.BrokerProvider.Broker = broker;
        this.MessageQueue = queue;

    }



    void Awake() {
        //these are default, isolated objects created for use within a single scene.
        // for cross-scene chatter, use Constuct to IMMEDIATELY replace them with others.
        Dictionary<Type, ICollection<Action<Message>>> channelMap = new Dictionary<Type, ICollection<Action<Message>>>();
        var queue = new Queue<Message>();
        this.BrokerProvider.Broker = new SubscriptionBroker(channelMap);

        this.Processor = new MessageProcessor(queue, channelMap, processAsSupertypes:false);
        this.MessageQueue = queue;

    }



    private void Update() {
        //TODO -- make this not awful, actually work, not just be a test case

        while (this.MessageQueue.Count > 0) {
            this.Processor.FireMessage(this.MessageQueue.Dequeue());
        }
    }




}

public class MessageProcessor : TypeFilteredMessageProcessor<Message> {
    public MessageProcessor(Queue<Message> queue, IDictionary<Type, ICollection<Action<Message>>> channelMap, bool processAsSupertypes) : base(queue, channelMap, processAsSupertypes) {
    }

}
