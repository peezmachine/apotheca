﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Messaging;
using EzPz.Collections;
using System;

public class SubscriptionBrokerProvider : MonoBehaviour {


    public SubscriptionBroker Broker;







}

public class SubscriptionBroker : SubscriptionBroker<Message, Type> {



    //this defintion 1) provides non-generic version for Unity niceness and 2) auto-generates channels with subscriptions

    public SubscriptionBroker(Dictionary<Type, ICollection<Action<Message>>> channelMap) : base(
        subscribeAction: delegate (Action<Message> subscriber, Type key) {
            ICollection<Action<Message>> channel;
            if (channelMap.TryGetValue(key, out channel)) {
                channelMap[key].Add(subscriber);
            } else {
                channelMap[key] = new NodeList<Action<Message>>() { subscriber };
            }
        }, unsubscribeAction: delegate (Action<Message> subscriber, Type key) {
            ICollection<Action<Message>> channel;
            if (channelMap.TryGetValue(key, out channel)) {
                channelMap[key].Remove(subscriber);
            }
        }
    ) { }





}