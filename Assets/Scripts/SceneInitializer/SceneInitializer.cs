﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Messaging;





public class SceneInitializer : MonoBehaviour {



    [SerializeField] MessageProcessorProvider _messageManagerProvider; //provides connected processor and broker


    public MessageProcessorProvider MessageManagerProvider {
        get {
            return _messageManagerProvider;
        }

        private set {
            _messageManagerProvider = value;
        }
    }



    public void Configure(MessageProcessor messageManager, SubscriptionBroker subscriptionManager, Queue<Message> queue) {
        this.MessageManagerProvider.Construct(messageManager, subscriptionManager, queue);
                



    }





}
