﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzPz.Collections.Observable;

public class SolitaireMatchModel : MonoBehaviour {


    [SerializeField] Dictionary<Vector2Int, Potion> _supply; // XXX -- this is solitaire-specific


    [SerializeField]
    Dictionary<Vector2Int, Potion> _boardPotions = new Dictionary<Vector2Int, Potion>(16); //backing ONLY!

    ObservableDictionary<Potion.PotionColor, Apothecary> _observableAlley;

    public bool UseTestLibrary;

    public int Score;
    public Player Player;

    //public CollectionChangedNotifier<KeyValuePair<Vector2Int, Potion>> PotionsChangedNotifier;
    public CollectionChangedNotifier<KeyValuePair<Potion.PotionColor, Apothecary>> AlleyChangedNotifier;


    //public ICollection<Potion> Potions { get { return this.BoardPotions.Values; } }
    public IDictionary<Vector2Int, Potion> Potions { get { return _boardPotions; } }
    public IDictionary<Vector2Int, Potion> Supply { get { return _supply; } private set { _supply = value as Dictionary<Vector2Int, Potion>;  }  }

    public IDictionary<Potion.PotionColor, Apothecary> ApothecariesForSale { get { return _observableAlley; } }


    public Stack<Apothecary> ApothecaryDeck;

    public int MovesLeft = 2;

    public int BoardSize = 4;

 

    private void Awake() {
        _observableAlley = new ObservableDictionary<Potion.PotionColor, Apothecary>(3);
        ApothecaryLibrary apothecaryLibrary;
        if (this.UseTestLibrary) {
            apothecaryLibrary = Resources.Load<ApothecaryLibrary>("Data/Libraries/TestLibrary");
        } else {
            apothecaryLibrary = Resources.Load<ApothecaryLibrary>("Data/Libraries/DefaultLibrary");
        }

        var apothecaries = new List<Apothecary>(apothecaryLibrary.Apothecaries.Count);

        foreach(ApothecaryData data in apothecaryLibrary.Apothecaries) {
            GameObject entity = new GameObject(data.Name);
            var apothecary = entity.AddComponent<Apothecary>();
            apothecary.Data = data;
            entity.transform.SetParent(this.transform);
            apothecaries.Add(apothecary);
        }

        apothecaries.Shuffle();
        this.ApothecaryDeck = new Stack<Apothecary>(apothecaries);



        //DEFAULT GAME SETTING STUFF, MOVE EVENTUALLY
        this.Supply  = new Dictionary<Vector2Int, Potion>(16);

        Stack<Potion> potionsDeck;
        {
            var potionsList = new List<Potion>(45);
            for (int i = 0; i < 45; i++) {


                Potion.PotionColor potionColor;
                if (i < 15) {
                    potionColor = Potion.PotionColor.Red;
                } else if (i < 30) {
                    potionColor = Potion.PotionColor.Blue;
                } else {
                    potionColor = Potion.PotionColor.Yellow;
                }

                Potion potion = this.MakePotion(potionColor);
                potion.transform.SetParent(this.transform);
                potion.name += i;
                potionsList.Add(potion);

            }
            //shuffle the deck
            potionsList.Shuffle();
            potionsDeck = new Stack<Potion>(potionsList);
        }



        //deal potions
        this.DealPotion(potionsDeck, new Vector2Int(0, 0));
        this.DealPotion(potionsDeck, new Vector2Int(3, 0));
        this.DealPotion(potionsDeck, new Vector2Int(0, 3));
        this.DealPotion(potionsDeck, new Vector2Int(3, 3));
        this.DealPotion(potionsDeck, new Vector2Int(0, 1), false);
        this.DealPotion(potionsDeck, new Vector2Int(1, 2), false);
        this.DealPotion(potionsDeck, new Vector2Int(2, 3), false);
        this.DealPotion(potionsDeck, new Vector2Int(1, 0), false);
        this.DealPotion(potionsDeck, new Vector2Int(2, 1), false);
        this.DealPotion(potionsDeck, new Vector2Int(3, 2), false);

        this.DealSupplyPotions(potionsDeck);


        //set up observable notifiers
        this.AlleyChangedNotifier = new CollectionChangedNotifier<KeyValuePair<Potion.PotionColor, Apothecary>>(_observableAlley);


    }

    private void Start() {
        //deal player an apothecary
        this.Player.Apothecaries.Add(this.ApothecaryDeck.Pop());
        //this.Player.Apothecaries.Add(this.ApothecaryDeck.Pop());
        //this.Player.Apothecaries.Add(this.ApothecaryDeck.Pop());

        //deal an apothecary to the alley slots
        this.ApothecariesForSale[Potion.PotionColor.Blue] = this.ApothecaryDeck.Pop();
        this.ApothecariesForSale[Potion.PotionColor.Red] = this.ApothecaryDeck.Pop();
        this.ApothecariesForSale[Potion.PotionColor.Yellow] = this.ApothecaryDeck.Pop();
    }

    void DealSupplyPotions(Stack<Potion> deck) {
        for(int x =0; x <4; x++) {
            for(int y = 0; y <4; y++) {
                Potion potion = deck.Pop();
                potion.IsRevealed = false;
                this.Supply[new Vector2Int(x, y)] = potion;
                
            }
        }
    }

    //Part of the default game stuff, remove eventually
    Potion MakePotion(Potion.PotionColor color) {
        GameObject potionEntity = new GameObject();
        Potion potion = potionEntity.AddComponent<Potion>();
        potion.Color = color;
        return potion;
    }

    //Part of the default game stuff, remove eventually
    void DealPotion(Stack<Potion> deck, Vector2Int position, bool isRevelead = true) {
        Potion potion = deck.Pop();
        //potion.Owner = this.Player;
        potion.IsRevealed = isRevelead;
        this.Potions[position] = potion;
    }
}
