﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using VoidEventHandler = Utility.VoidEventHandler;
using EzPz.Collections.Observable;


public class Player : MonoBehaviour {



    public ObservableList<Apothecary> Apothecaries = new ObservableList<Apothecary>();

    [SerializeField]
    ColorToIntDictionary _gems = new ColorToIntDictionary {
        { Potion.PotionColor.Red, 0},
        { Potion.PotionColor.Blue, 0},
        { Potion.PotionColor.Yellow, 0}
    };



    public ObservableDictionary<Potion.PotionColor, int>  Gems;


    void Awake() {
        Gems = new ObservableDictionary<Potion.PotionColor, int>(_gems);

    }


    [System.Serializable]
    public class ColorToIntDictionary :  SerializableDictionaryBase<Potion.PotionColor, int> { }


}
